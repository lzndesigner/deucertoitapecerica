<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstablishmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('establishments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('proprietary', 255);
            $table->integer('category_id');
            $table->text('description')->nullable();
            $table->string('cep');
            $table->string('address');
            $table->string('number');
            $table->string('distric');
            $table->string('city');
            $table->string('state');
            $table->string('country');
            $table->string('phone');
            $table->string('cellphone')->nullable();
            $table->integer('views')->nullable()->default('0');
            $table->string('slug', 100)->unique();
            $table->index('slug');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('establishments');
    }
}
