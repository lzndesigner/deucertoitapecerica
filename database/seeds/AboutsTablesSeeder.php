<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Models\About;

class AboutsTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('abouts')->delete();

        About::create([
            'page_description'=>'Somos um catalogo online dos estabelecimentos da cidade de Itapecerica.',
        ]);
    }
}
