<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Models\Config;

class ConfigsTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configs')->delete();

        Config::create([
            'config_title' => 'Deu certo em Itapecerica',
            'config_description' => 'Somos um catalogo online dos estabelecimentos da cidade de Itapecerica.',
            'config_keywords' => 'deu, certo, itapecerica, sao, paulo',
            'config_email' => 'contato@deucerto.com.br',
            'config_phone' => '(00) 0000-0000',
            'config_cellphone' => '(00) 0000-0000',
            'config_information' => '',
            'company_name' => 'Deu certo em Itapecerica',
            'company_proprietary' => 'Juliana',
            'company_address' => 'Endereço completo',
            'company_city' => 'Itapecerica',
            'company_state' => 'São Paulo',
            'company_country' => 'BR',
            'redesocial_facebook' => '#',
            'redesocial_instagram' => '#',
            'redesocial_twitter' => '#',
        ]);
    }
}
