<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessUploadJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    protected $filename;

    /**
     * @var string
     */
    protected $buffer;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($filename, $buffer)
    {
        //
        $this->filename = $filename;
        $this->buffer = $buffer;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        app('filesystem')->drive('s3')->put($this->filename, base64_decode($this->buffer), 'public');
    }
}
