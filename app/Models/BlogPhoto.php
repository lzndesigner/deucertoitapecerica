<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogPhoto extends Model
{
    protected $fillable = ['filename', 'sizes'];

    /**
     * @var array
     */
    protected $casts = [
        'sizes' => 'collection'
    ];

    public function blog()
    {
        return $this->belongsTo('App\Models\Blog');
    }

    public function url($size = 100)
    {
        $disk = app('filesystem')->drive('s3');

        return "https://deucertoitapecerica.s3-sa-east-1.amazonaws.com/" . $this->sizes->get($size);
    }

    public function hasSize(int $size): bool 
    {
        return $this->sizes->has($size);
    }
}
