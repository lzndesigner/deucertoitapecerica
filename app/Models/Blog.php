<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = [
    	'title',
    	'body',
    	'slug',
    	'tags',
    	'status'
    ];

    protected $appends = [
        'photo_default'
    ];

    public function photos()
    {
    	// o laravel trabalha com pluralidade das models

    	// Um blog cotem uma ou mais fotos? mais fotos, ento temos uma relaçao 1:n 1(blog) : n (fotos)
    	// quando usar essa relaço um para muitos ou "tem muitos para um" nomeie as tabelas
    	// assim: 1 "blog" : n "fotos" = blog_photos como o model possui a pluralidade ele faz a transformaço. Deu para compreender? Deu sim, precisamos mudar isso. Onde foi implementado ambas as models? Isso? aram, s na controller? isso.
    	return $this->hasMany('App\Models\BlogPhoto'); // espero que tenha compreendido,  sim um aprendizado valioso rs
    }
    
    public function getPhotoDefaultAttribute() 
    {
        if($this->photos->isEmpty()) return '/sem_image.png';

        $photo = $this->photos()->first();
        
        return $photo->has(256) ? $photo->url(256) : '/sem_image.png';
    }
}