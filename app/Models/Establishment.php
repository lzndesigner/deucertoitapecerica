<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Establishment extends Model
{
    protected $fillable = [
        'name',
        'proprietary',
        'category_id',
        'description',
        'image',
        'cep',
        'address',
        'number',
        'distric',
        'city',
        'state',
        'country',
        'phone',
        'phone_whatsapp',
        'cellphone',
        'email',
        'website',
        'slug',
        'status'
    ];

    public function getPostCategory(){
        return Category::where('id', $this->category_id)->first()->title;
    }
    public function getPostCategorySlug(){
        return Category::where('id', $this->category_id)->first()->slug;
    }

    public function photos()
    {
        // o laravel trabalha com pluralidade das models

        // Um blog cotem uma ou mais fotos? mais fotos, ento temos uma relaçao 1:n 1(blog) : n (fotos)
        // quando usar essa relaço um para muitos ou "tem muitos para um" nomeie as tabelas
        // assim: 1 "blog" : n "fotos" = blog_photos como o model possui a pluralidade ele faz a transformaço. Deu para compreender? Deu sim, precisamos mudar isso. Onde foi implementado ambas as models? Isso? aram, s na controller? isso.
        return $this->hasMany('App\Models\EstablishmentPhoto'); // espero que tenha compreendido,  sim um aprendizado valioso rs
    }
}