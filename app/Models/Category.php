<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    public function establishments()
	{
		return $this->hasMany('Establishment');
	}
    protected $fillable = [
    	'title',
    	'slug',
    	'status',
    	'image'
    ];


    public function photos()
    {
        return $this->hasMany('App\Models\CategoryPhoto'); // espero que tenha compreendido,  sim um aprendizado valioso rs
    }
}
