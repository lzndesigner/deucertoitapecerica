<?php

namespace App\Mail;

use App\Models\Newsletter;
use App\Models\NewsletterConfirmation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConfirmSubscriberNewsletter extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var NewsletterConfirmation
     */
    private $confirmation;

    /**
     * ConfirmSubscriberNewsletter constructor.
     * @param NewsletterConfirmation $confirmation
     */
    public function __construct(NewsletterConfirmation $confirmation)
    {
        $this->confirmation = $confirmation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Confirme sua inscrição!')
			->from(config('mail.newsletter.address'),config('mail.newsletter.name'))
            ->to($this->confirmation->newsletter->email, $this->confirmation->newsletter->name)
            ->markdown('mail.site.newsletter', ['dados' => $this->confirmation]);
    }
}
