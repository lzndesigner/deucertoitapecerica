<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\URL;

use View;
use DB;

use App\Models\Config;
use App\Models\About;
use App\Models\Slider;
use App\Models\Blog;
use App\Models\BlogPhoto;
use App\Models\Review;
use App\Models\Category;
use App\Models\Establishment;
use App\Models\Calendar;

use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        if (DB::connection()->getDatabaseName()) {
            View::share('configs', Config::first());
            View::share('abouts', About::first());
            View::share('sliders', Slider::get());
            View::share('blogs', Blog::where('status', 1)->orderBy('created_at', 'DESC')->get());
            View::share('categories', Category::orderBy('title', 'ASC')->get());
            View::share('establishments', Establishment::where('status', 1)->get());
            View::share('reviews', Review::where('status', 1)->get());

            //View::share('calendars', Calendar::query()->eventsInOpened()->get()->sortBy('day')->sortBy('month')->all());
            View::share('calendars', Calendar::orderBy('month', 'ASC')->orderBy('day', 'ASC')->get());
            View::share('establishmentsColumns', Establishment::orderBy('created_at', 'DESC')->get());
            View::share('calendarsColumns', Calendar::orderBy('month', 'ASC')->orderBy('day', 'ASC')->get());
            \Carbon\Carbon::setLocale('pt-br');
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
