<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
class ReservationDetailCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'body' => $this->body,
            'slug' => $this->slug,
            'image' => $this->image,
            'created_at' => $this->created_at->format('d-m-Y H:m'),
            'updated_at' => $this->updated_at->diffForHumans(),
        ];
    }
}
