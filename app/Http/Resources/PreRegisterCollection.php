<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
class PreRegisterCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'subject' => $this->subject,
            'status' => $this->getOriginal('status'),
            'note' => $this->note,
            'city' => $this->city,
            'state' => $this->state,
            'reservation' => $this->whenLoaded('reservation', $this->reservation),
            'formatted' => [
                'status' => $this->status
            ],
            'created_at' => $this->created_at->format('d-m-Y H:m'),
            'updated_at' => $this->updated_at->diffForHumans(),
        ];
    }
}
