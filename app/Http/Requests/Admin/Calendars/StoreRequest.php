<?php

namespace App\Http\Requests\Admin\Calendars;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => ['required', 'min:5'],
            'day' => ['required'], 
            'month' => ['required'], 
            'year' => ['required'], 
            'hour' => ['required'], 
            'local' => ['required'], 
            'classification' => ['required'], 
            'slug' => ['required']
        ];

        $photos =  (is_array($this->input('photos')) ? count($this->input('photos')) : 0);

        foreach(range(0, $photos) as $index) {
            $rules['photos.' . $index] = 'image|mimes:jpeg,bmp,png|max:2000';
        }
 
        return $rules;

    }

    public function attributes() {
        return [
            'name' => 'titulo',
            'day' => 'dia',
            'month' => 'mês',
            'year' => 'ano',
            'hour' => 'hora',
            'local' => 'local',
            'classification' => 'classificação',
            'slug' => 'url',
        ];
    }
    
    public function messages()
        {
            // voce refez as mensagens por que? Porque o tratamendo delas usa o name="" para informar o erro. exemplo, o campo config_title não foi preenchido, acho isso para o Cliente final meio estranho.
            // tem uma slução
            return [
                'name.required' => 'O título é necessário.',
                'day.required' => 'O Dia é necessário.',
                'month.required' => 'O Mês é necessário.',
                'year.required' => 'O Ano é necessário.',
                'hour.required' => 'A Hora é necessário.',
                'local.required' => 'O Local é necessário.',
                'classification.required' => 'A Classificação é necessário.',
                'slug.required' => 'A URL amigável é necessário.',
            ];
        }
}
