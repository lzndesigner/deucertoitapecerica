<?php

namespace App\Http\Requests\Admin\Reviews;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'name' => ['required', 'min:3'],
            'charge' => ['required'],
            'status' => ['required'],
            'body' => ['required', 'min:30'],
        ];
    }

    public function attributes() {
        return [
            'name' => 'Nome',
            'charge' => 'Cargo',
            'status' => 'Status',
            'body' => 'Descrição'
        ];
    }
    
    public function messages()
        {
            return [
                'name.required' => 'O nome é necessário.',
                'charge.required' => 'O cargo/profissão é necessário.',
                'status.required' => 'O Status é necessário.',
                'body.required' => 'O depoimento é necessário.'
            ];
        }
}
