<?php

namespace App\Http\Requests\Admin\Users;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'name' => ['required', 'min:5'],
            'email' => ['required', 'min:5'],
            'level' => ['required'],
        ];
    }

    public function attributes() {
        return [
            'name' => 'Nome',
            'email' => 'E-mail',
            'level' => 'Nível'
        ];
    }
    
    public function messages()
        {
            return [
                'name.required' => 'O nome é necessário.',
                'email.required' => 'O e-mail é necessário.',
                'level.required'  => 'O nível é necessário.',
            ];
        }
}
