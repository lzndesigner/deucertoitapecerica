<?php

namespace App\Http\Requests\Admin\Blogs;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [
                'required', 
                'min:5',
                Rule::unique('blogs')->ignore($this->route('blog'))
            ],
            'slug' => ['required'], 
            'body' => ['required', 'min:30'],
            'status' => ['required']
        ];
    }
}
