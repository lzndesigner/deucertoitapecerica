<?php

namespace App\Http\Requests\Admin\Establishments;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => ['required', 'min:4'],
            'proprietary' => ['required', 'min:4'],
            'category_id' => ['required'],
            // 'cep' => ['required'],
            // 'address' => ['required'],
            // 'number' => ['required'],
            // 'distric' => ['required'],
            'city' => ['required'],
            'state' => ['required'],
            'country' => ['required'],
            'phone' => ['required'],
            'slug' => ['required'],
            'status' => ['required']
        ];

        $photos =  (is_array($this->input('photos')) ? count($this->input('photos')) : 0);

        foreach(range(0, $photos) as $index) {
            $rules['photos.' . $index] = 'image|mimes:jpeg,bmp,png|max:2000';
        }

        return $rules;

    }

    public function attributes() {
        return [
            'name' => 'estabelecimento',
            'proprietary' => 'proprietário',
            'category_id' => 'categoria',
            // 'cep' => 'CEP',
            // 'address' => 'endereço',
            // 'number' => 'número',
            // 'distric' => 'bairro',
            'city' => 'cidade',
            'state' => 'estado',
            'country' => 'país',
            'phone' => 'telefone',
            'slug' => 'slug',
            'status' => 'status'
        ];
    }
    
    public function messages()
    {
            // voce refez as mensagens por que? Porque o tratamendo delas usa o name="" para informar o erro. exemplo, o campo config_title não foi preenchido, acho isso para o Cliente final meio estranho.
            // tem uma slução
        return [
            'name.required' => 'O estabelecimento é necessário.',
            'proprietary.required' => 'O proprietário é necessário.',
            'category_id.required' => 'A categoria é necessário.',
            // 'cep.required' => 'O CEP é necessário.',
            // 'address.required' => 'O endereço é necessário.',
            // 'number.required' => 'O número é necessário.',
            // 'distric.required' => 'O bairro é necessário.',
            'city.required' => 'A cidade é necessário.',
            'state.required' => 'O estado é necessário.',
            'country.required' => 'O país é necessário.',
            'phone.required' => 'O telefone é necessário.',
            'slug.required' => 'A URL é necessário.',
            'status.required' => 'O Status é necessário.',
        ];
    }
}
