<?php

namespace App\Http\Requests\Admin\Establishments;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [ 
            'name' => [
                'required', 
                'min:5',
                Rule::unique('establishments')->ignore($this->route('establishment'))
            ],
            'proprietary' => ['required', 'min:4'],
            'category_id' => ['required'],
            // 'cep' => ['required'],
            // 'address' => ['required'],
            // 'number' => ['required'],
            // 'distric' => ['required'],
            'city' => ['required'],
            'state' => ['required'],
            'country' => ['required'],
            'phone' => ['required'],
            'slug' => ['required'],
            'status' => ['required']
        ];
    }
}
