<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Calendar;

class CalendarsController extends Controller
{
	 /**
     * Armazena uma nova instancia do model Calendar
     *
     * @var \App\Calendar
     */
	 private $calendars;

    /**
     * Metodo construtor.
     */
    public function __construct()
    {
    	$this->calendars = app(Calendar::class);
    }

    public function index()
    {
    	return view('front.calendars');
    }

    public function showCalendar(Calendar $calendar)
    {
    	$calendar = Calendar::where('id', $calendar->id)->first();
        $calendar->increment('views');
        return view('front.calendars_show', compact('calendar'));
    }
}
