<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\Establishment;

class CategoriesController extends Controller
{
	 /**
     * Armazena uma nova instancia do model Establishment
     *
     * @var \App\Establishment
     */
	 private $categories;
	 private $establishments;

    /**
     * Metodo construtor.
     */
    public function __construct()
    {
    	$this->categories = app(Establishment::class);
    	$this->establishments = app(Establishment::class);
    }

    public function index()
    {
    	return view('front.categories');
    }

    public function show(Category $category)
    {
    	$establishments = Establishment::where('category_id', $category->id)->get();
    	return view('front.categories_show', compact('category', 'establishments'));
    }

    public function showEstablishment(Category $category, Establishment $establishment)
    {
    	$establishment = Establishment::where('category_id', $category->id)->where('id', $establishment->id)->first();
        $establishment->increment('views');
    	return view('front.establishment_show', compact('category', 'establishment'));
    }
}
