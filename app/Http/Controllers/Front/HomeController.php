<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\Establishment;
use App\Models\Blog;
use App\Models\Calendar;
use App\Support\SearchSupport;

class HomeController extends Controller
{
    public function suspenso()
    {
     
    }

    public function index()
    {
        $establishmentsAlls = Establishment::orderBy('created_at', 'DESC')->get();

        return view('front.home', compact('establishmentsAlls'));
    }
    public function maintence()
    {
        return view('front.maintence');
    }
    /**
     * @param Request $request
     */
    public function busca(Request $request)
    {
        $support = SearchSupport::find($request->input('buscar'));

        $result = $support->mountQueries();
        // dd($result);
        //
        return view('front.resultado-de-busca', $result);
    }
}
