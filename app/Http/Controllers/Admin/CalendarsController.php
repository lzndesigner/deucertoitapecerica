<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\Calendars\StoreRequest;
use App\Http\Requests\Admin\Calendars\UpdateRequest;

use App\Models\Calendar;
use App\Models\CalendarPhoto;

use Image;

class CalendarsController extends Controller
{
    /**
     * Armazena uma nova instancia do model Calendar
     *
     * @var \App\Calendar
     */
    private $calendars;

    /**
     * Metodo construtor.
     */
    public function __construct()
    {
      $this->calendars = app(Calendar::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $calendars = Calendar::orderBy($request->input('sort', 'created_at'), 'ASC')->paginate();
      return view('admin.calendars.index', compact('calendars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin.calendars.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        try {
        app('db')->beginTransaction(); // inicia uma transaçao

        $calendar = Calendar::create($request->all()); // nesse momento o registro ja foi criado.
        
        foreach ($request->photos as $photo) {
          $filename = $photo->store("calendars/{$calendar->id}/photos");
            // associa fotos ao calendar recem criado.
          $calendar->photos()->create(['filename' => $filename]);
        }

        app('db')->commit(); // confirma transaço

        session()->flash('messages.success', ['Calendário cadastrado com sucesso!']);
        return redirect()->route('calendars.index');
      }catch(\Execpetion $error) {
        //
        app('db')->rollback(); // reverte transacao

        session()->flash('messages.error', ['Houve um erro. Tente novamente!']);
        return redirect()->route('calendars.index');
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $calendar = Calendar::findOrFail($id);
      return view('admin.calendars.show', compact('calendar'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $calendar = Calendar::findOrFail($id);
      return view('admin.calendars.edit', compact('calendar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRequest $request, $id)
    {
      $calendar = $this->calendars->where('id', $id)->first();

      if(!$calendar) {
        session()->flash('messages.error', ['Calendário não existe!']);
        return redirect()->route('calendars.index');
      }

      $calendar->fill($request->all()); // armazena atributos

      $photos = $request->photos; // arquivos

      //
      // Caso exista fotos na ediçao
      //
      if(is_array($photos) && count($photos) > 0) {
        foreach ($photos as $id => $photo) {
          //
          // Verificar se existe foto informada na lista de fotos pertencentes ao calendar.
          //
          if($calendar->photos->contains('id', $id)) {
            //
            // registro atual
            //
            $row = $calendar->photos->find($id);

            $old = $row->filename;

            $row->filename = $photo->store("calendars/{$calendar->id}/photos");

            $row->save(); // atualiza

            if($row->isDirty() && app('filesystem')->exists($old)) {
              app('filesystem')->delete($old);
            }

            continue;
          }
        }
      }

      if($request->hasFile('addPhotos')) {
        //
        // Adicionar novas fotos
        //
        $photos = $request->addPhotos;
      
        foreach ($photos as $file) {
          $filename = $file->store("calendars/{$calendar->id}/photos");
          
          if($filename) {
            //
            // Adicionar foto ao calendar
            //
            $calendar->photos()->create(compact('filename'));

            continue;
          }
        }
      }

      $calendar->save(); // guarda alteraçoes

      session()->flash('messages.success', ['Postagem alterada com sucesso!']);
      return redirect()->route('calendars.index');

  }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Calendar $calendar)
    {

     $db = app('db');

     $db->beginTransaction();

     try {
      $id = $calendar->id;

      $calendar->delete();

      // remover diretorio
      app('filesystem')->delete("calendars/{$id}");

      $db->commit();

      return response(null, 204);
     }catch(\Exception $error) {
       $db->rollback();
       return response()->json(['message' => $error->getMessage()], 400);
     }     
    
    }


  public function photoRemove(Calendar $calendar, $photo_id)
  {
      //
      // verificar se existe relacionamento
      //
      if(!$calendar->photos->contains('id', $photo_id)) abort(403, 'Essa imagem não pertence ao Calendário.');
    
      $photo = $calendar->photos->find($photo_id);

      app('db')->beginTransaction();
      
      try {
        $filename = $photo->filename;

        $photo->delete();

        if(app('filesystem')->exists($filename)) {
          // remover do diretorio
          app('filesystem')->delete($filename);
        }

        app('db')->commit();

        return response(null, 204);
      }catch(\Exception $error) {
        app('db')->rollback();
        return response()->json(['message' => $error->getMessage()], 400);
      }
  }




  }
