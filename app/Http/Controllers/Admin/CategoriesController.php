<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\Categories\StoreRequest;
use App\Http\Requests\Admin\Categories\UpdateRequest;

use App\Models\Category;
use App\Models\CategoryPhoto;

use Image;

class CategoriesController extends Controller
{
    /**
     * Armazena uma nova instancia do model Category
     *
     * @var \App\Category
     */
    private $categories;

    /**
     * Metodo construtor.
     */
    public function __construct()
    {
      $this->categories = app(Category::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $categories = Category::orderBy($request->input('sort', 'created_at'), 'ASC')->paginate();
      return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
      try {
        app('db')->beginTransaction(); // inicia uma transaçao

        $category = Category::create($request->all()); // nesse momento o registro ja foi criado.
        
        foreach ($request->photos as $photo) {
          $filename = $photo->store("categories/{$category->id}/photos");
            // associa fotos ao category recem criado.
          $category->photos()->create(['filename' => $filename]);
        }

        app('db')->commit(); // confirma transaço

        session()->flash('messages.success', ['Categoria cadastrada com sucesso!']);
        return redirect()->route('categories.index');
      }catch(\Execpetion $error) {
        //
        app('db')->rollback(); // reverte transacao

        session()->flash('messages.error', ['Houve um erro. Tente novamente!']);
        return redirect()->route('categories.index');
      }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $category = Category::findOrFail($id);
      return view('admin.categories.show', compact('category'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $category = Category::findOrFail($id);
      return view('admin.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRequest $request, $id)
    {
     $category = $this->categories->where('id', $id)->first();

     if(!$category) {
      session()->flash('messages.error', ['Categoria não existe!']);
      return redirect()->route('categories.index');
    }

      $category->fill($request->all()); // armazena atributos

      $photos = $request->photos; // arquivos

      //
      // Caso exista fotos na ediçao
      //
      if(is_array($photos) && count($photos) > 0) {
        foreach ($photos as $id => $photo) {
          //
          // Verificar se existe foto informada na lista de fotos pertencentes ao category.
          //
          if($category->photos->contains('id', $id)) {
            //
            // registro atual
            //
            $row = $category->photos->find($id);

            $old = $row->filename;

            $row->filename = $photo->store("categories/{$category->id}/photos");

            $row->save(); // atualiza

            if($row->isDirty() && app('filesystem')->exists($old)) {
              app('filesystem')->delete($old);
            }

            continue;
          }
        }
      }

      if($request->hasFile('addPhotos')) {
        //
        // Adicionar novas fotos
        //
        $photos = $request->addPhotos;
        
        foreach ($photos as $file) {
          $filename = $file->store("categories/{$category->id}/photos");
          
          if($filename) {
            //
            // Adicionar foto ao category
            //
            $category->photos()->create(compact('filename'));

            continue;
          }
        }
      }

      $category->save(); // guarda alteraçoes

      session()->flash('messages.success', ['Categoria alterada com sucesso!']);
      return redirect()->route('categories.index');
    }

    public function photoRemove(Category $category, $photo_id)
    {
      //
      // verificar se existe relacionamento
      //
      if(!$category->photos->contains('id', $photo_id)) abort(403, 'Essa imagem não pertence a Categoria.');
      
      $photo = $category->photos->find($photo_id);

      app('db')->beginTransaction();
      
      try {
        $filename = $photo->filename;

        $photo->delete();

        if(app('filesystem')->exists($filename)) {
          // remover do diretorio
          app('filesystem')->delete($filename);
        }

        app('db')->commit();

        return response(null, 204);
      }catch(\Exception $error) {
        app('db')->rollback();
        return response()->json(['message' => $error->getMessage()], 400);
      }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Category $category)
    {

      $category->delete();

      return response(null, 204);
    //return response()->redirectToRoute('categories.index');
    }
  }
