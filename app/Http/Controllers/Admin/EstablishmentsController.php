<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\Establishments\StoreRequest;
use App\Http\Requests\Admin\Establishments\UpdateRequest;

use App\Models\Establishment;
use App\Models\EstablishmentPhoto;

use Image;
use App\Support\UploadSupport;

class EstablishmentsController extends Controller
{
    /**
     * Armazena uma nova instancia do model Establishment
     *
     * @var \App\Establishment
     */
    private $establishments;

    /**
     * Metodo construtor.
     */
    public function __construct()
    {
      $this->establishments = app(Establishment::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $establishments = Establishment::orderBy($request->input('sort', 'created_at'), 'ASC')->paginate();
      return view('admin.establishments.index', compact('establishments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin.establishments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
      try {
        app('db')->beginTransaction(); // inicia uma transaçao
  
        $data = $request->all();
  
        if (isset($data['phone_whatsapp'])) {
          $data['phone_whatsapp'] = 1;
        } else {
          $data['phone_whatsapp'] = 0;
        }
  
        $establishment = Establishment::create($data); // nesse momento o registro ja foi criado.
  
        foreach ($request->photos as $photo) {
          // dd($photo);
          $filename = $photo->store("establishments/{$establishment->id}/photos");
          // associa fotos ao establishment recem criado.
          $establishment->photos()->create(['filename' => $filename]);
        }
  
        app('db')->commit(); // confirma transaço
  
        session()->flash('messages.success', ['Estabelecimento cadastrado com sucesso!']);
        return redirect()->route('establishments.index');
      } catch (\Execpetion $error) {
        //
        app('db')->rollback(); // reverte transacao
  
        session()->flash('messages.error', ['Houve um erro. Tente novamente!']);
        return redirect()->route('establishments.index');
      }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $establishment = Establishment::findOrFail($id);
      return view('admin.establishments.show', compact('establishment'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $establishment = Establishment::findOrFail($id);
      return view('admin.establishments.edit', compact('establishment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRequest $request, $id)
    {
      $establishment = $this->establishments->where('id', $id)->first();

      if(!$establishment) {
        session()->flash('messages.error', ['Establecimento não existe!']);
        return redirect()->route('establishments.index');
      }

      $data = $request->all();

      if (isset($data['phone_whatsapp'])) {
        $data['phone_whatsapp'] = 1;
      } else {
        $data['phone_whatsapp'] = 0;
      }

      $establishment->fill($data); // armazena atributos

      $photos = $request->photos; // arquivos

      //
      // Caso exista fotos na ediçao
      //
      if(is_array($photos) && count($photos) > 0) {
        foreach ($photos as $id => $photo) {
          //
          // Verificar se existe foto informada na lista de fotos pertencentes ao establishment.
          //
          if($establishment->photos->contains('id', $id)) {
            //
            // registro atual
            //
            $row = $establishment->photos->find($id);

            $old = $row->filename;

            $row->filename = $photo->store("establishments/{$establishment->id}/photos");

            $row->save(); // atualiza

            if($row->isDirty() && app('filesystem')->exists($old)) {
              app('filesystem')->delete($old);
            }

            continue;
          }
        }
      }

      if($request->hasFile('addPhotos')) {
        //
        // Adicionar novas fotos
        //
        $photos = $request->addPhotos;

        foreach ($photos as $file) {
          $filename = $file->store("establishments/{$establishment->id}/photos");
          
          if($filename) {
            //
            // Adicionar foto ao establishment
            //
            $establishment->photos()->create(compact('filename'));

            continue;
          }
        }
      }

      $establishment->save(); // guarda alteraçoes

      session()->flash('messages.success', ['Establecimento alterado com sucesso!']);
      return redirect()->route('establishments.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Establishment $establishment)
    {

     $db = app('db');

     $db->beginTransaction();

     try {
      $id = $establishment->id;

      $establishment->delete();

      // remover diretorio
      app('filesystem')->delete("establishments/{$id}");

      $db->commit();

      return response(null, 204);
    }catch(\Exception $error) {
     $db->rollback();
     return response()->json(['message' => $error->getMessage()], 400);
   }
 }



 public function photoRemove(Establishment $establishment, $photo_id)
 {
      //
      // verificar se existe relacionamento
      //
  if(!$establishment->photos->contains('id', $photo_id)) abort(403, 'Essa imagem não pertence ao Estabelecimento.');

  $photo = $establishment->photos->find($photo_id);

  app('db')->beginTransaction();

  try {
    $filename = $photo->filename;

    $photo->delete();

    if(app('filesystem')->exists($filename)) {
          // remover do diretorio
      app('filesystem')->delete($filename);
    }

    app('db')->commit();

    return response(null, 204);
  }catch(\Exception $error) {
    app('db')->rollback();
    return response()->json(['message' => $error->getMessage()], 400);
  }
}


}
