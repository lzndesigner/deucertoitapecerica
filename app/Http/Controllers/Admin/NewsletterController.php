<?php

namespace App\Http\Controllers\Admin;

use App\Exports\NewslettersExport;
use App\Models\Newsletter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\NewsletterCollection;
use Maatwebsite\Excel\Facades\Excel;

class NewsletterController extends Controller
{
    /**
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $newsletters = NewsletterCollection::collection(Newsletter::query()->orderBy('status', 'desc')->get());

        return $newsletters;
    }

    /**
     * Gerar lista de emails separados por virgula
     *
     * @param Request $request
     * @return mixed
     */
    public function generate(Request $request)
    {
        $outputDir = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
        $fileName = 'newsletters-' . now()->getTimestamp() . '.xls';
        $export = new NewslettersExport();
        // salvar no disco
        Excel::store($export, $fileName, 'exports', \Maatwebsite\Excel\Excel::XLS);
        // download
        return Excel::download($export, $fileName, \Maatwebsite\Excel\Excel::XLS);
    }

    /**
     * Remover email da lista
     *
     * @param Newsletter $newsletter
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function destroy(Newsletter $newsletter)
    {
        $newsletter->delete();

        return response(null, 204);
    }
}
