<?php

namespace App\Support;

use Intervention\Image\Facades\Image;
use Intervention\Image\ImageManagerStatic;
use App\Jobs\ProcessUploadJob;
use Illuminate\Filesystem\Filesystem;

class UploadSupport
{
    /**
     * Gerar thumbs
     * 
     * @var array
     */
    protected $sizes = [
        100,
        256,
        512,
        1024
    ];

    /**
     * Diretorio padrao
     * 
     * @var string
     */
    protected $folder = 'blogs';

    /**
     * @var
     */
    protected $file;

    /**
     * Processar imagens
     * 
     * @var array
     */
    protected $pipes;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $blog_id;

    /**
     * @param $file
     */
    private function __construct($file)
    {
        $this->file = $file;
        $this->name = \str_random(8);
    }

    /**
     * @return UploadSupport
     */
    public static function make($file): UploadSupport
    {
        return new self($file);
    }

    /**
     * Preparar
     * @return $this
     */
    public function prepare(string $blog_id)
    {
        // Id blog
        $this->blog_id = $blog_id;

        // processar tarefas
        $this->preparePipeThumbs();

        return $this;
    }

    /**
     * Retornar lista com nome de arquivos
     * 
     * @return array
     */
    public function getSizesFilename(): array
    {
        $names = [];

        foreach ($this->sizes as $size) {
            $names[$size] = $this->replaceName("{$this->folder}/:id/{$this->name}/w_{$size}.webp");
        }

        return $names;
    }

    /**
     * Obter nome do caminho relativo.
     * 
     * @return string
     */
    public function getPath(): string
    {
        return $this->replaceName("{$this->folder}/:id/{$this->name}");
    }

    /**
     * Formatar nome
     * 
     * @return string
     */
    public function replaceName($value)
    {
        return str_replace(':id', $this->blog_id, $value);
    }

    /**
     * Montar imagens de acordo com o tamanho definido.
     * 
     * @return void
     */
    public function preparePipeThumbs(): void
    {
        foreach ($this->sizes as $size) {
            //
            $image = ImageManagerStatic::make($this->file);

            //
            $image->resize($size, null, function ($constrait) {
                $constrait->aspectRatio();
                $constrait->upSize();
            });

            //
            $this->pipes[$size] = [
                'filename' => $this->replaceName("{$this->folder}/:id/{$this->name}/w_{$size}.webp"),
                'buffer' => $image->encode('webp', 100)
            ];
        }
    }

    /**
     * Empilhar
     * 
     * @return void
     */
    public function dispatch()
    {
        foreach ($this->pipes as $pipe) {
            //
            //
            //
            ProcessUploadJob::dispatch($pipe['filename'], $pipe['buffer']);
        }
    }

    /**
     * Empilhar
     * 
     * @return void
     */
    public function dispatchNow()
    {

        foreach ($this->pipes as $pipe) {
            $upload = self::drive()->put($pipe['filename'], $pipe['buffer'], 'public');
            
            if(!$upload) continue;
        }
    }

    /**
     * Obtem instancia do drive utilizado na operação.
     * 
     * @return Filesystem
     */
    public static function drive()
    {
        return app('filesystem')->drive('s3');
    }
}
