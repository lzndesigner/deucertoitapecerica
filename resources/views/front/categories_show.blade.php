@extends('front.base')
@section('title', $category->title)
@section('breadcrumb')
<div id="section-header" class="section parallax nomargin notopborder dark skrollable skrollable-between" style="background-image: url('/galerias/bg_section_home_2.jpg?3'); padding: 50px 0px 250px; background-position: 0px 31.5574px;" data-bottom-top="background-position:0px 300px;" data-top-bottom="background-position:0px -300px;">
  <div class="container clearfix">
    <div class="emphasis-title center">
      <h1>@yield('title')</h1>
    </div>

    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li><a href="{{ url('/categorias') }}">Categorias</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>
  @include('front.includes.box-search')
</div><!-- section-home -->
@endsection

@section('content')
<!-- START CONTAINER -->
<section id="section-home" class="section sub-page">
  <div class="container">
    <div class="box-general">
      <div class="row">

        <div class="col-xs-12 col-md-12">
          @if(count($establishments) > 0)
          @foreach($establishments as $establishment)
          <div class="col-md-4">
            <div class="testimonial">
              <div class="testi-image">
                <a href="/categorias/{{ $establishment->getPostCategorySlug() }}/{{ $establishment->slug }}">
                  <img src="{{ $establishment->photos->isNotEmpty() ? asset('storage/' . $establishment->photos[0]->filename) : '' }}" alt="{{ $establishment->name }}">
                </a>
              </div>
              <div class="testi-content">
                <div class="testi-meta">
                  <h2>{{ $establishment->name }}</h2>
                  <span>- {{ $establishment->getPostCategory() }}</span>
                </div>
              </div>
            </div>
          </div><!-- col-md-4 -->
          @endforeach
          @else
          <div class="alert alert-info">
            Não há estabelecimentos cadastrados nesta categoria.
          </div>
          @endif

      </div><!--col-md-9 -->

    </div><!-- row -->
  </div><!-- box-general -->

</div><!-- container -->
</section>
<!-- END CONTAINER -->

@include('front.includes.box-categories')

@endsection