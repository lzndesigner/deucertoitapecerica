<li class=""><a href="{{ url('/sobre') }}"><div>Sobre</div></a></li>

@if($categories->count() > 0)
<li class="sub-menu">
	<a href="#"><div>Categorias <i class="fa fa-angle-down"></i></div></a>
	<ul>
		@foreach($categories as $category)
		<li class=""><a href="/categorias/{{ $category->slug }}"><div>{{ $category->title }}</div></a></li>
		@endforeach
	</ul>
</li>
@endif
<li class=""><a href="{{ url('/calendario') }}"><div>Calendário</div></a></li>
<li class=""><a href="{{ url('/blogs') }}"><div>Blog</div></a></li>
<li class=""><a href="{{ url('/contato') }}"><div>Contato</div></a></li>