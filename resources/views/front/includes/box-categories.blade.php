<div class="divider divider-short divider-center"><i class="icon-circle-blank"></i></div>

<div id="section-category-home" style="padding:20px 0;">
  <div class="container clearfix">
    <div class="box-category-home"  style="width:100%; margin:0;">
      <h3>Veja outras Categorias</h3>
      <div id="oc-images" class="owl-carousel image-carousel carousel-widget" data-autoplay="5000" data-margin="20" data-loop="true" data-nav="true" data-pagi="true" data-items-xxs="1" data-items-xs="2" data-items-sm="6" data-items-md="6" data-items-lg="6">

        @if($categories->count() > 0)
        @foreach($categories as $category)
        <div class="oc-item item-category">
          <a href="/categorias/{{ $category->slug }}">
            <picture><img src="{{ $category->photos->isNotEmpty() ? asset('storage/' . $category->photos[0]->filename) : asset('storage/sem_image.png')}}" alt="{{ $category->title }}"></picture>
            <h2>{{ $category->title }}</h2>
          </a>
        </div>
        @endforeach
        @endif
      </div>
    </div>
  </div>
</div>
<!-- END CONTAINER -->
