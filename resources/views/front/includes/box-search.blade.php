<div id="section-category-home" style="padding:20px 0;">
  <div class="container clearfix">
    <div id="top-search">
      <form action="{{ url('/estabelecimentos/busca') }}" method="post">
        {{ csrf_field() }}
        <input type="text" name="buscar" class="form-control" value="" placeholder="O que você está buscando em Itapecerica?" required>
        <button type="submit"><i class="fa fa-search"></i></button>
      </form>
    </div><!-- #top-search end -->
  </div>
</div>
<!-- END CONTAINER -->