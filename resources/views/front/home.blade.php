@extends('front.base')
@section('title', '')
@section('jsPage')
@endsection

@section('content')
<!-- START CONTAINER -->
<div id="section-header" class="section parallax nomargin notopborder dark skrollable skrollable-between" style="background-image: url('/galerias/bg_section_home_2.jpg?3'); padding: 50px 0px 250px; background-position: 0px 31.5574px;" data-bottom-top="background-position:0px 300px;" data-top-bottom="background-position:0px -300px;">
    <div class="container clearfix">
      <div class="emphasis-title center">
        <h1>Guia de Negócios <br> em Itapecerica</h1>
    </div>
    <div class="row">

        <div id="top-search">
          <h3>Faça sua pesquisa</h3>
          <form action="{{ url('/estabelecimentos/busca') }}" method="post">
            {{ csrf_field() }}
            <input type="text" name="buscar" class="form-control" value="" placeholder="O que você está buscando em Itapecerica?" required>
            <button type="submit"><i class="fa fa-search"></i></button>
        </form>
    </div><!-- #top-search end -->

</div>
</div>
</div><!-- section-home -->


<div id="section-category-home">
  <div class="container clearfix">
      <div class="box-category-home-shadow"></div>
      <div class="box-category-home">
          <h3>Busque por Categorias</h3>
          <div id="oc-images" class="owl-carousel image-carousel carousel-widget" data-autoplay="5000" data-margin="20" data-loop="true" data-nav="true" data-pagi="true" data-items-xxs="1" data-items-xs="2" data-items-sm="4" data-items-md="4" data-items-lg="4">

              @if($categories->count() > 0)
              @foreach($categories as $category)
              <div class="oc-item item-category">
                  <a href="/categorias/{{ $category->slug }}">
                      <picture><img src="{{ $category->photos->isNotEmpty() ? asset('storage/' . $category->photos[0]->filename) : asset('storage/sem_image.png')}}" alt="{{ $category->title }}"></picture>
                      <h2>{{ $category->title }}</h2>
                  </a>
              </div>
              @endforeach
              @endif
          </div>
      </div>
  </div>
</div>


<div id="section-home">
    <div class="container clearfix">
        <div class="emphasis-title center">
            <h2 style="font-size: 38px;">Conheça os Estabelecimentos</h2>
            <h3>veja os melhores locais da cidade</h3>
        </div>
        <div class="divider divider-short divider-center"><i class="icon-circle-blank"></i></div>
        <div class="row">

            <div id="oc-images" class="owl-carousel image-carousel carousel-widget" data-autoplay="7000" data-margin="10" data-loop="true" data-nav="true" data-pagi="true" data-items-xxs="1" data-items-xs="2" data-items-sm="4" data-items-md="4" data-items-lg="4">


                @if(count($establishmentsAlls) > 0)
                @foreach($establishmentsAlls as $establishmentsAll)

                <div class="oc-item bottommargin">
                    <div class="testimonial">
                        <div class="testi-image">
                            <a href="/categorias/{{ $establishmentsAll->getPostCategorySlug() }}/{{ $establishmentsAll->slug }}">
                                <img src="{{ $establishmentsAll->photos->isNotEmpty() ? asset('storage/' . $establishmentsAll->photos[0]->filename) : '' }}" alt="{{ $establishmentsAll->name }}">
                            </a>
                        </div>
                        <div class="testi-content">
                            <div class="testi-meta">
                                <h2>{{ $establishmentsAll->name }}</h2>
                                <span>- {{ $establishmentsAll->getPostCategory() }}</span>
                            </div>
                        </div>
                    </div>
                </div><!-- col -->

                @endforeach
                @endif

            </div>
        </div>
    </div>
</div><!-- section-home -->

@if(count($calendars) > 0)
<div id="section-calendar" class="section parallax nomargin notopborder dark skrollable skrollable-between" style="background:#1d1d1d; padding: 50px 0px; margin:100px 0 !important; background-position: 0px 31.5574px;" data-bottom-top="background-position:0px 300px;" data-top-bottom="background-position:0px -300px;">
    <div class="container clearfix">
        <div class="row">
            <div class="emphasis-title center">
                <h2 style="font-size: 38px;">Calendário de Eventos</h2>
                <h3>não fique de fora dos eventos</h3>
            </div>                      

            @foreach($calendars as $calendar)
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="team">
                    <div class="team-date">
                        {{ $calendar->day }}/{{ $calendar->month }}
                    </div>
                    <div class="team-image">
                        <a href="/calendario/{{ $calendar->slug }}"><img src="{{ $calendar->photos->isNotEmpty() ? asset('storage/' . $calendar->photos[0]->filename) : '' }}" alt="{{ $calendar->name }}"></a>
                        <span class="badge badge-theme">{{ $calendar->classification }} @if($calendar->classification != 'livre') anos @endif</span>
                    </div>
                    <div class="team-desc">
                        <div class="team-title">
                            <h4><a href="/calendario/{{ $calendar->slug }}">{{ $calendar->name }}</a></h4>
                            <span>{{ $calendar->local }}</span>
                        </div>
                        <div class="team-buttons">
                            <a href="/calendario/{{ $calendar->slug }}" class="button button-small button-border button-rounded button-fill button-theme"><span>Informações</span></a>
                        </div>
                        
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div><!-- section-calendar -->
@endif


<!-- <div class="section-redesociais">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="widget">
                    <h4><i class="icon-instagram"></i> Nosso Instagram</h4>
                    <iframe src="https://snapwidget.com/embed/643244" class="snapwidget-widget" allowtransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:100%; "></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
 -->

@if(count($sliders) > 0)
<div class="col_full bottommargin-md">
    <div class="fslider flex-thumb-grid grid-6" data-animation="fade" data-arrows="true" data-dots="true" data-thumbs="false">
        <div class="flexslider">
            <div class="slider-wrap">
                @foreach($sliders as $slider)
                <div class="slide" data-thumb="/storage/{{ $slider->image }}">
                    @if($slider->url != '#')
                    <a href="{{ $slider->url }}">
                        @endif
                        <img src="/storage/{{ $slider->image }}" alt="{{ $slider->title}}">
                        <div class="overlay">
                            <div class="text-overlay">
                                <div class="text-overlay-title">
                                    <h3>{{ $slider->title}}</h3>
                                </div>
                            </div>
                        </div>
                        @if($slider->url)
                    </a>@endif
                </div><!-- slide -->
                @endforeach
            </div>
        </div>
    </div><!-- fslider -->
</div><!-- col Full -->

<div class="clear"></div>
@endif


@if(count($blogs) > 0)
<div id="section-blog">
    <div class="container clearfix">
        <div class="row">
            <div class="emphasis-title center">
                <h2 style="font-size: 38px;">Blog de Itapecerica</h2>
                <h3>confira as novidades da cidade</h3>
            </div>
            <div class="divider divider-short divider-center"><i class="icon-circle-blank"></i></div>


            <div class="blogsAll">
                @foreach($blogs as $blog)
                <div class="col-xs-12 col-md-3 bottommargin-sm">
                    <div class="feature-box media-box">
                        <div class="fbox-media">
                            <a href="/blogs/{{ $blog->slug }}"><img src="{{ $blog->photos->isNotEmpty() ? $blog->photos[0]->url(256) : 'sem_image.png' }}" alt="{{ $blog->title }}"></a>
                        </div>
                        <div class="fbox-desc">
                            <h3><a href="/blogs/{{ $blog->slug }}">{{ str_limit($blog->title, $limit = 85, $end = '...') }}</a></h3>
                        </div>
                    </div>
                </div><!-- col-xs-12 col-md-4 -->
                @endforeach
            </div>
        </div>
    </div>
</div><!-- section-calendar -->
@endif


<!-- END CONTAINER -->
@endsection