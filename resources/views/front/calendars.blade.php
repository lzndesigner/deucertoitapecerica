@extends('front.base')
@section('title', 'Calendário')

@section('breadcrumb')
<div id="section-header" class="section parallax nomargin notopborder dark skrollable skrollable-between" style="background-image: url('/galerias/bg_section_home_2.jpg?3'); padding: 50px 0px 250px; background-position: 0px 31.5574px;" data-bottom-top="background-position:0px 300px;" data-top-bottom="background-position:0px -300px;">
  <div class="container clearfix">
    <div class="emphasis-title center">
      <h1>@yield('title')</h1>
    </div>

    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>
  @include('front.includes.box-search')
</div><!-- section-home -->
@endsection

@section('content')
<!-- START CONTAINER -->
<section id="section-calendar" class="section sub-page light">
  <div class="container">
    <div class="box-general" style="padding:40px 20px 0;">
      <div class="row">

        @if(count($calendars) > 0)
        @foreach($calendars as $calendar)
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="team">
            <div class="team-date">
              {{ $calendar->day }}/{{ $calendar->month }}
            </div>
            <div class="team-image">
              <a href="/calendario/{{ $calendar->slug }}"><img src="{{ $calendar->photos->isNotEmpty() ? asset('storage/' . $calendar->photos[0]->filename) : '' }}" alt="{{ $calendar->name }}"></a>
              <span class="badge badge-theme">{{ $calendar->classification }} @if($calendar->classification != 'livre') anos @endif</span>
            </div>
            <div class="team-desc">
              <div class="team-title">
                <h4><a href="/calendario/{{ $calendar->slug }}">{{ $calendar->name }}</a></h4>
                <span>{{ $calendar->local }}</span>
              </div>
              <div class="team-buttons">
                <a href="/calendario/{{ $calendar->slug }}" class="button button-small button-border button-rounded button-fill button-theme"><span>Informações</span></a>
              </div>

            </div>
          </div>
        </div>
        @endforeach
        @else
        <div class="alert alert-info"><p>Não há nenhum Evento cadastrado em nosso Calendário no momento.</p></div>
        @endif

      </div><!-- row -->
    </div><!-- row -->
  </div><!-- container -->
</section>

@include('front.includes.box-categories')

<!-- END CONTAINER -->
@endsection