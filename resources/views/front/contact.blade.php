@extends('front.base')
@section('title', 'Entre em Contato')
@section('breadcrumb')
<div id="section-header" class="section parallax nomargin notopborder dark skrollable skrollable-between" style="background-image: url('/galerias/bg_section_home_2.jpg?3'); padding: 50px 0px 250px; background-position: 0px 31.5574px;" data-bottom-top="background-position:0px 300px;" data-top-bottom="background-position:0px -300px;">
  <div class="container clearfix">
    <div class="emphasis-title center">
      <h1>@yield('title')</h1>
    </div>

    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>
  @include('front.includes.box-search')
</div><!-- section-home -->
@endsection

@section('content')
<section class="section sub-page" id="oc-contact">
  <div class="container">
    <div class="box-general">
      <div class="row">
        <div class="col-xs-12 col-md-7">

          <div class="fancy-title title-dotted-border">
            <h3>Entre em Contato</h3>
          </div>

          <div class="contact-widget">

            <div class="contact-form-result"></div>

            <form-contato></form-contato>
          </div><!-- contact-widget -->

        </div><!-- col-xs-12 col-md-7 -->

        <div class="col-xs-12 col-md-5 center">
          <div class="col-xs-12 col-sm-12 col-md-12 bottommargin clearfix">
            <div class="feature-box fbox-center fbox-bg fbox-plain">
              <div class="fbox-icon">
                <a href="#"><i class="icon-phone3"></i></a>
              </div>
              <h3>Telefone
                <span class="subtitle">{{$configs->config_phone}}</span>
                @if(!empty($configs->config_cellphone))
                <span class="subtitle">
                  {{$configs->config_cellphone}}
                </span>
                @endif</h3>
              </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 bottommargin clearfix">
              <div class="feature-box fbox-center fbox-bg fbox-plain">
                <div class="fbox-icon">
                  <a href="#"><i class="icon-envelope"></i></a>
                </div>
                <h3>E-mail<span class="subtitle"><a href="mailto:{{$configs->config_email}}">{{$configs->config_email}}</a></span></h3>
              </div>
            </div>
          </div>

        </div><!-- row -->
      </div><!-- box-general -->

    </div><!-- container -->
  </section>

  <!-- END CONTAINER -->
  @endsection