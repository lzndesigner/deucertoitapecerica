<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="{{ $configs->config_description ?? '' }}">
  <meta name="keywords" content="{{ $configs->config_keywords ?? '' }}" />
  <meta property="og:title" content="{{ $configs->config_title ?? 'Deu Certo em Itapecerica' }}" />
  <meta property="og:description" content="{{ $configs->config_description ?? '' }}" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="{{Request::url()}}" />
  <meta property="og:image" content="https://deucertoitapecerica.com.br/facebook.png" />
  <meta property="og:locale" content="pt_BR" />
  <meta property="og:type" content="website" />

  <title>{{ $configs->config_title ?? 'Deu Certo em Itapecerica' }}</title>
  <base href="{{Request::url()}}" />
  <link rel="canonical" href="{{Request::url()}}" />

  <link href="/favicon.ico?1" rel="shortcut icon" />
  @php
  header('Set-Cookie: cross-site-cookie=name; SameSite=None; Secure');
  @endphp
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Stylesheets
  ============================================= -->
  <link href="https://fonts.googleapis.com/css?family=Poppins|Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="{{ asset('front/css/app.css?4') }}" type="text/css" />
  <!--[if lt IE 9]>
  <script src="https://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
  <![endif]-->

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-138517094-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-138517094-1');
  </script>
  <!-- end:Global site tag (gtag.js) - Google Analytics -->

  @yield('cssPage')
</head>

<body class="stretched no-transition">

  <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "Organization",
      "name": "Deu Certo Itapecerica",
      "url": "https://www.deucertoitapecerica.com.br",
      "sameAs": [
        "https://www.facebook.com/deucertoemitap",
        "https://www.instagram.com/deucertoitapecerica"
      ],
      "address": {
        "@type": "PostalAddress",
        "streetAddress": "Av. XV de Novembro",
        "addressRegion": "Itapecerica da Serra - São Paulo",
        "postalCode": "06850100",
        "addressCountry": "BR"
      }
    }
  </script>


  <div id="app">
    <!-- Document Wrapper
  ============================================= -->
    <div id="wrapper" class="clearfix">

      <div class="">

        <div class="boxed">
          <a href="https://api.whatsapp.com/send?phone=5511934897896" target="_Blank" class="box-whatsapp"><img src="data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 64 64' aria-labelledby='title' aria-describedby='desc' role='img' xmlns:xlink='http://www.w3.org/1999/xlink'%3E%3Ctitle%3EWhatsApp%3C/title%3E%3Cdesc%3EA line styled icon from Orion Icon Library.%3C/desc%3E%3Cpath data-name='layer2' d='M30.287 2.029A29.769 29.769 0 0 0 5.223 45.266L2.064 60.6a1.158 1.158 0 0 0 1.4 1.361L18.492 58.4A29.76 29.76 0 1 0 30.287 2.029zm17.931 46.2' fill='none' stroke='%23ffffff' stroke-linecap='round' stroke-miterlimit='10' stroke-width='3' stroke-linejoin='round'%3E%3C/path%3E%3Cpath data-name='layer1' d='M46.184 38.205l-5.765-1.655a2.149 2.149 0 0 0-2.126.561l-1.41 1.436a2.1 2.1 0 0 1-2.283.482c-2.727-1.1-8.463-6.2-9.927-8.754a2.1 2.1 0 0 1 .166-2.328l1.23-1.592a2.148 2.148 0 0 0 .265-2.183l-2.424-5.485a2.149 2.149 0 0 0-3.356-.769c-1.609 1.361-3.517 3.428-3.749 5.719-.409 4.039 1.323 9.13 7.872 15.242 7.566 7.063 13.626 8 17.571 7.04 2.238-.542 4.026-2.714 5.154-4.493a2.15 2.15 0 0 0-1.218-3.221z' fill='none' stroke='%23ffffff' stroke-linecap='round' stroke-miterlimit='10' stroke-width='3' stroke-linejoin='round'%3E%3C/path%3E%3C/svg%3E" alt="WhatsApp" /> fale conosco</a>

          <!-- Header
        ============================================= -->
          <header id="header" class="sticky-style-2">

            <div class="container clearfix">
              <div class="row">
                <div class="col-xs-12 col-md-3">
                  <div id="logo">
                    <a href="{{ url('/')}}" class="standard-logo" data-dark-logo="{{ asset('logo_branca.png') }}"><img src="{{ asset('logo_preto.png') }}" alt="Deu Certo em Itapecerica"></a>
                    <a href="{{ url('/')}}" class="retina-logo" data-dark-logo="{{ asset('logo_branca.png') }}"><img src="{{ asset('logo_preto.png') }}" alt="Deu Certo em Itapecerica"></a>
                  </div><!-- #logo end -->
                </div><!-- col-md-3 -->
                <div class="col-xs-12 col-md-6 pull-right">
                  <div id="header-wrap">
                    <nav id="primary-menu" class="style-2">
                      <div id="primary-menu-trigger">MENU <i class="icon-reorder"></i></div>
                      <ul>
                        @include('front.includes.menu-top')
                      </ul>
                    </nav><!-- #primary-menu end -->
                  </div><!-- header-wrap -->
                  <div class="publicity-header">
                    <ul>
                      <li><a href="http://www.kiwimidia.com/" target="_Blank"><img src="{{ asset('storage/publicity/kiwimidia.gif') }}" alt="Kiwimidia Identidade Visual"></a></li>
                    </ul>
                  </div>
                </div><!-- col-md-6 -->
              </div><!-- row -->
            </div><!-- container -->


            <div id="top-social">
              <ul>
                @if($configs->redesocial_facebook != '#')
                <li><a href="{{ $configs->redesocial_facebook }}" class="colored-facebook si-facebook"><span class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span></a></li>
                @endif

                @if($configs->redesocial_instagram != '#')
                <li><a href="{{ $configs->redesocial_instagram }}" class="colored-instagram si-instagram"><span class="ts-icon"><i class="icon-instagram2"></i></span><span class="ts-text">Instagram</span></a></li>
                @endif

                @if($configs->redesocial_twitter != '#')
                <li><a href="{{ $configs->redesocial_twitter }}" class="colored-twitter si-twitter"><span class="ts-icon"><i class="icon-twitter"></i></span><span class="ts-text">{{ $configs->redesocial_twitter }}</span></a></li>
                @endif

                <li><a href="mailto:{{ $configs->config_email}}" class="colored-email3 si-email3"><span class="ts-icon"><i class="icon-email3"></i></span><span class="ts-text">{{ $configs->config_email}}</span></a></li>
              </ul>
            </div><!-- #top-social end -->


          </header><!-- #header end -->

          <!-- Content
  ============================================= -->
          <section id="content">
            <div class="content-wrap">
              @yield('breadcrumb')

              @yield('content')
            </div><!-- .content-wrap -->
          </section><!-- #content -->
          <!-- End Content -->


          <!-- Footer
  ============================================= -->
          <footer id="footer" class="dark">

            <div class="container">

              <div class="row">

                <!-- Footer Widgets
        ============================================= -->
                <div class="footer-widgets-wrap clearfix">

                  <div class="col-xs-12 col-md-3">

                    <div class="widget clearfix">
                      <img src="{{ asset('logo_branca.png') }}" alt="Deu Certo em Itapecerica" class="footer-logo">
                    </div>
                    <div class="fb-page" data-href="https://www.facebook.com/deucertoemitap/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                      <blockquote cite="https://www.facebook.com/deucertoemitap/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/deucertoemitap/">Deu Certo em Itap</a></blockquote>
                    </div>

                  </div>

                  <div class="col-xs-12 col-md-3">
                    <div class="widget widget_links clearfix">

                      <h4>Categorias</h4>

                      @foreach($categories as $category)
                      <li class=""><a href="/categorias/{{ $category->slug }}">
                          <div>{{ $category->title }}</div>
                        </a></li>
                      @endforeach
                    </div>
                  </div><!-- cols -->

                  <div class="col-xs-12 col-md-6">

                    <div class="row">
                      <div class="col-xs-12 col-md-6">
                        <div class="widget widget_links clearfix">

                          <h4>Navegação</h4>

                          <li class=""><a href="{{ url('/') }}">
                              <div>Início</div>
                            </a></li>
                          <li class=""><a href="{{ url('/sobre') }}">
                              <div>Sobre</div>
                            </a></li>
                          <li class=""><a href="{{ url('/calendario') }}">
                              <div>Calendário</div>
                            </a></li>
                          <li class=""><a href="{{ url('/blogs') }}">
                              <div>Blog</div>
                            </a></li>
                          <li class=""><a href="{{ url('/contato') }}">
                              <div>Entre em Contato</div>
                            </a></li>
                        </div>
                      </div><!-- cols -->

                      <div class="col-xs-12 col-md-6">
                        <h4>Entre em Contato</h4>
                        <div>
                          <abbr title="E-mail"><strong>E-mail:</strong></abbr> <a href="mailto:{{ $configs->config_email }}">{{ $configs->config_email }}</a>
                          <ul class="footer-phones">
                            @if(!empty($configs->config_phone))<li><strong>Telefone: </strong>{{$configs->config_phone}} </li> @endif
                            @if(!empty($configs->config_cellphone)) <li><strong>Celular: </strong>{{$configs->config_cellphone}} </li> @endif
                          </ul>
                        </div>

                        <h4>Siga-nos</h4>

                        <div class="fleft clearfix">
                          @if($configs->redesocial_facebook != '#')
                          <a href="{{ $configs->redesocial_facebook }}" data-toggle="tooltip" data-original-title="Facebook" class="social-icon si-small si-borderless si-facebook">
                            <i class="icon-facebook"></i>
                            <i class="icon-facebook"></i>
                          </a>
                          @endif

                          @if($configs->redesocial_instagram != '#')
                          <a href="{{ $configs->redesocial_instagram }}" data-toggle="tooltip" data-original-title="Instagram" class="social-icon si-small si-borderless si-instagram">
                            <i class="icon-instagram"></i>
                            <i class="icon-instagram"></i>
                          </a>
                          @endif

                          @if($configs->redesocial_twitter != '#')
                          <a href="{{ $configs->redesocial_twitter }}" data-toggle="tooltip" data-original-title="Twitter" class="social-icon si-small si-borderless si-twitter">
                            <i class="icon-twitter"></i>
                            <i class="icon-twitter"></i>
                          </a>
                          @endif

                        </div>

                      </div><!-- cols -->

                    </div>
                    <div class="row">
                      <div class="publicity-header footer">
                        <ul>
                          <li><a href="http://www.kiwimidia.com/" target="_Blank"><img src="{{ asset('storage/publicity/kiwimidia.gif') }}" alt="Kiwimidia Identidade Visual"></a></li>
                        </ul>
                      </div>
                    </div>

                  </div><!-- col-md-6 -->

                </div><!-- .footer-widgets-wrap end -->

              </div><!-- row -->
            </div><!-- container -->

            <!-- Copyrights
    ============================================= -->
            <div id="copyrights">
              <div class="container clearfix">
                <div class="col_half">
                  Todos os direitos reservados á <b>Deu Certo em Itapecerica</b> {{ date('Y') }}
                  <br>
                  <small>Programação: <a href="https://innsystem.com.br" title="InnSystem Inovação em Sistemas" taget="_Blank">InnSystem</a></small>
                </div>
              </div>

            </div><!-- #copyrights end -->

          </footer><!-- #footer end -->

        </div><!-- boxed -->

      </div><!-- container -->

    </div><!-- #wrapper end -->

    <!-- Go To Top
  ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

  </div>
  <!-- External JavaScripts
  ============================================= -->
  <script src="{{ asset('js/app.js') }}"></script>
  <script type="text/javascript" src="{{ asset('front/js/jquery.js') }}"></script>
  <script defer src="{{ asset('front/js/fontawesome-all.js') }}"></script>
  <script type="text/javascript" src="{{ asset('front/js/plugins.js?3') }}"></script>
  <script type="text/javascript" src="{{ asset('front/js/functions.js') }}"></script>
  @yield('jsPage')
  <!-- SnapWidget -->
  <script src="https://snapwidget.com/js/snapwidget.js"></script>

  <div id="fb-root"></div>
  <script>
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s);
      js.id = id;
      js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v3.1&appId=1094654680570525&autoLogAppEvents=1';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  </script>
  <script src="https://snapwidget.com/js/snapwidget.js"></script>
</body>

</html>