@extends('front.base')
@section('title', 'Blog de Itapecerica')
@section('breadcrumb')
<div id="section-header" class="section parallax nomargin notopborder dark skrollable skrollable-between" style="background-image: url('/galerias/bg_section_home_2.jpg?3'); padding: 50px 0px 250px; background-position: 0px 31.5574px;" data-bottom-top="background-position:0px 300px;" data-top-bottom="background-position:0px -300px;">
  <div class="container clearfix">
    <div class="emphasis-title center">
      <h1>@yield('title')</h1>
    </div>

    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li><a href="{{ url('/blogs') }}">Todas publicações</a></li>
      <li class="active">@yield('title')</li>
    </ol>

  </div>
  @include('front.includes.box-search')
  
</div><!-- section-home -->
@endsection

@section('content')
<section id="section-blog" class="section sub-page">
  <div class="container">
    <div class="box-general">
      <div class="row">

        <div class="col-xs-12 col-md-12">


          <div class="row blogsAll">
            @if(count($blogs) > 0)
            @foreach($blogs as $blog)
            <div class="col-xs-12 col-md-4 bottommargin-sm">
              <div class="feature-box media-box">
                <div class="fbox-media">
                  <a href="/blogs/{{ $blog->slug }}"><img src="{{ $blog->photos->isNotEmpty() ? $blog->photos[0]->url(256) : 'sem_image.png' }}" alt="{{ $blog->title }}"></a>
                  <!-- inverter negação ! if -->
                </div>
                <div class="fbox-desc">
                  <h3><a href="/blogs/{{ $blog->slug }}">{{ str_limit($blog->title, $limit = 120, $end = '...') }}</a></h3>
                </div>
              </div>
            </div><!-- col-xs-12 col-md-4 -->
            @endforeach
            @else
            <div class="alert alert-info">
              <p>Não há postagens cadastradas no momento.</p>
            </div>
            @endif
          </div><!-- row -->
        </div><!-- col-md-9 -->
      </div><!-- row -->
    </div><!-- box-general -->

  </div><!-- container -->
</section>
<!-- END CONTAINER -->
@include('front.includes.box-categories')
@endsection