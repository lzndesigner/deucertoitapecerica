@extends('front.base')
@section('title', 'Resultado da Busca')
@section('breadcrumb')
<div id="section-header" class="section parallax nomargin notopborder dark skrollable skrollable-between" style="background-image: url('/galerias/bg_section_home_2.jpg?3'); padding: 50px 0px 250px; background-position: 0px 31.5574px;" data-bottom-top="background-position:0px 300px;" data-top-bottom="background-position:0px -300px;">
    <div class="container clearfix">
        <div class="emphasis-title center">
            <h1>@yield('title')</h1>
        </div>

        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}">Início</a></li>
            <li class="active">@yield('title')</li>
        </ol>
    </div>
    @include('front.includes.box-search')
</div><!-- section-home -->
@endsection

@section('content')
<!-- START CONTAINER -->
<div class="section sub-page">
    <div class="container clearfix">
        <div class="box-general">
            <div class="row">

                <div class="col-md-12">

                    <div class="col_full clearfix allEstablishments">



                        <div class="tabs clearfix" id="tab-1">
                            <ul class="tab-nav clearfix">
                                @if(count($establishment) > 0)
                                <li><a href="#tabs-1"><i class="icon-home2 norightmargin"></i> Estabelecimentos</a></li>
                                @endif
                                @if(count($blog) > 0)
                                <li><a href="#tabs-2"><i class="fa fa-comments"></i> Blog de Itapecerica</a></li>
                                @endif
                                @if(count($calendar) > 0)
                                <li><a href="#tabs-3"><i class="fa fa-calendar"></i> Calendário</a></li>
                                @endif
                            </ul>
                            <div class="tab-container">
                                @if(count($establishment) > 0)
                                <div class="tab-content clearfix" id="tabs-1">
                                    <div id="section-home" class="page-search row">
                                        @foreach($establishment as $result)
                                        <div class="col-xs-12 col-md-3 bottommargin">
                                            <div class="testimonial">
                                                <div class="testi-image">
                                                    <a href="/categorias/{{ $result->getPostCategorySlug() }}/{{ $result->slug }}">
                                                        <img src="{{ $result->photos->isNotEmpty() ? asset('storage/' . $result->photos[0]->filename) : '' }}" alt="{{ $result->name }}">
                                                    </a>
                                                </div>
                                                <div class="testi-content">
                                                    <div class="testi-meta">
                                                        <h2>{{ $result->name }}</h2>
                                                        <span>- {{ $result->getPostCategory() }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- col -->
                                        @endforeach
                                    </div>
                                </div>
                                @endif

                                @if(count($blog) > 0)
                                <div class="tab-content clearfix" id="tabs-2">
                                    <div class="row blogsAll">
                                        @foreach($blog as $result)
                                        <div class="col-xs-12 col-md-4 bottommargin-sm">
                                            <div class="feature-box media-box">
                                                <div class="fbox-media">
                                                    <a href="/blogs/{{ $result->slug }}"><img src="{{ $result->photos->isNotEmpty() ? $result->photos[0]->url(256) : 'sem_image.png' }}" alt="{{ $result->title }}"></a>
                                                </div>
                                                <div class="fbox-desc">
                                                    <h3><a href="/blogs/{{ $result->slug }}">{{ str_limit($result->title, $limit = 85, $end = '...') }}</a></h3>
                                                </div>
                                            </div>
                                        </div><!-- col-xs-12 col-md-4 -->
                                        @endforeach
                                    </div>
                                </div>
                                @endif

                                @if(count($calendar) > 0)
                                <div class="tab-content clearfix" id="tabs-3">
                                    <div id="section-calendar" class="row light">
                                        @foreach($calendar as $result)
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <div class="team">
                                                <div class="team-date">
                                                    {{ $result->day }}/{{ $result->month }}
                                                </div>
                                                <div class="team-image">
                                                    <a href="/calendario/{{ $result->slug }}"><img src="{{ $result->photos->isNotEmpty() ? asset('storage/' . $result->photos[0]->filename) : '' }}" alt="{{ $result->name }}"></a>
                                                    <span class="badge badge-theme">{{ $result->classification }} @if($result->classification != 'livre') anos @endif</span>
                                                </div>
                                                <div class="team-desc">
                                                    <div class="team-title">
                                                        <h4><a href="/calendario/{{ $result->slug }}">{{ $result->name }}</a></h4>
                                                        <span>{{ $result->local }}</span>
                                                    </div>
                                                    <div class="team-buttons">
                                                        <a href="/calendario/{{ $result->slug }}" class="button button-small button-border button-rounded button-fill button-black"><span>Informações</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>


                        @if(count($establishment) > 0 && count($blog) > 0 && count($calendar) > 0)
                        @else
                        <hr>
                        <div class="alert alert-info">
                            @if(count($establishment) > 0)
                            @else
                            Não foi encontrado resultados em <b>Estabelecimentos</b>. <br>
                            @endif

                            @if(count($blog) > 0)
                            @else
                            Não foi encontrado resultados em <b>Blogs</b>. <br>
                            @endif

                            @if(count($calendar) > 0)
                            @else
                            Não foi encontrado resultados em <b>Calendários</b>. <br>
                            @endif
                        </div>
                        @endif


                    </div><!-- col-full -->

                </div><!-- col-md-9 -->

            </div><!-- row -->
        </div><!-- box-general -->
    </div><!-- container -->
</div>
<!-- END CONTAINER -->

@include('front.includes.box-categories')
@endsection