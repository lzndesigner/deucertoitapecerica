@extends('front.base')
@section('title', $establishment->name)
@section('breadcrumb')
<div id="section-header" class="section parallax nomargin notopborder dark skrollable skrollable-between" style="background-image: url('/galerias/bg_section_home_2.jpg?3'); padding: 50px 0px 250px; background-position: 0px 31.5574px;" data-bottom-top="background-position:0px 300px;" data-top-bottom="background-position:0px -300px;">
  <div class="container clearfix">
    <div class="emphasis-title center">
      <h1>@yield('title')</h1>
    </div>

    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li><a href="/categorias/{{ $establishment->getPostCategorySlug() }}">{{ $establishment->getPostCategory() }}</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>
  @include('front.includes.box-search')
</div><!-- section-home -->
@endsection

@section('content')
<!-- START CONTAINER -->


<section class="section sub-page">
  <div class="container">
    <div class="row">
      <div class="box-general">
        <div class="col-xs-12 col-md-12">

          <div class="col_full bottommargin-lg clearfix">
            <div class="postcontent nobottommargin clearfix">
              <div class="single-post nobottommargin">
                <div class="entry clearfix">

                  <div class="row">
                    <div class="col-xs-12 col-md-8">
                      <div class="entry-title">
                        <h1>{{ $establishment->name }}</h1>
                      </div><!-- entry-title -->

                      <ul class="entry-meta clearfix">
                        <li><a href="/categorias/{{ $establishment->getPostCategorySlug() }}"><i class="icon-plus"></i> {{ $establishment->getPostCategory() }}</a></li>
                        <li><i class="icon-calendar3"></i> Publicado em {{ $establishment->created_at->format('d/m/Y H:i:s') }}</li>
                        <li><i class="icon-eye"></i> Visualizações: {{ $establishment->views }}</li>
                      </ul>

                      <blockquote>
                        <div class="entry-content notopmargin" style="font-size:16px;">
                          {!! $establishment->description !!}
                        </div><!-- entry-content notopmargin -->                       

                      </blockquote>
                      <!--  -->
                      <div class="entry-image">
                        <ul id="lightSlider">
                          @foreach($establishment->photos as $photo)
                          <li data-thumb="{{ asset('storage/' . $photo->filename) }}">
                            <img src="{{ asset('storage/' . $photo->filename) }}" />
                          </li>
                          @endforeach
                        </ul>
                      </div>
                    </div><!-- col-md-6 -->

                    <div class="col-xs-12 col-md-4">

                      <div class="widget widget_links" style="margin:10px 0;">
                        <h4>Endereço:</h4>
                        <ul>
                          @if($establishment->address)
                          <li><i class="fa fa-map-marker"></i> {{ $establishment->address }}, N° {{ $establishment->number }}
                            @if($establishment->distric)
                            {{ $establishment->distric }}, {{ $establishment->city }}/{{ $establishment->state }}
                            @else
                            {{ $establishment->city }}/{{ $establishment->state }}                        
                            @endif
                          </li>
                          @endif

                          @php $retira = ["(", ")", " ", "-"]; @endphp
                          @if($establishment->phone_whatsapp == 1)
                          <li><i class="fab fa-whatsapp"></i> <a href="https://api.whatsapp.com/send?phone=55{{ str_replace($retira, '', $establishment->phone) }}" target="_Blank">{{ $establishment->phone }}</a></li>
                          @else
                          <li><i class="fa fa-phone"></i> <a href="tel:{{ $establishment->phone }}">{{ $establishment->phone }}</a></li>
                          @endif

                          @if($establishment->cellphone) 
                          <li><i class="fa fa-phone"></i> <a href="tel:{{ $establishment->cellphone }}">{{ $establishment->cellphone }}</a> </li>
                          @endif

                          @if($establishment->email)<li><i class="fa fa-envelope"></i> {{ $establishment->email }} </li>@endif
                          @if($establishment->website)<li><i class="fa fa-globe"></i> <a href="{{ $establishment->website }}" target="_Blank">{{ $establishment->website }}</a> </li>@endif
                        </ul>
                      </div>


                      @if($establishment->address)
                      <div class="widget widget_links" style="margin:10px 0;">
                        <h4>Mapa</h4>
                        <iframe
                        width="400"
                        height="350"
                        frameborder="0" style="border:0"
                        src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBKrjwCIuK892A4JTgfxzHN371ugnAZka4&q={!! str_replace(' ', '+', $establishment->name) !!},{!! str_replace(' ', '+', $establishment->city) !!}+{!! str_replace(' ', '+', $establishment->state) !!}&zoom=15" allowfullscreen>
                      </iframe>
                    </div>
                    @endif

                    <div class="widget widget_links" style="margin:10px 0;">
                      <h4>Comentários</h4>                                     
                      <div id="fb-root"></div>
                      <script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v5.0&appId=1094654680570525&autoLogAppEvents=1"></script>
                      <div class="fb-comments" data-href="{{Request::url()}}" data-width="350" data-numposts="8"></div>
                    </div> 


                  </div><!-- col-md-4 -->
                </div><!-- row -->

              </div><!-- entry clearfix -->
            </div><!-- single-post nobottommargin -->
          </div><!-- postcontent nobottommargin clearfix -->
        </div><!-- col full -->

      </div><!--col-md-12 -->
    </div><!-- box-general -->
  </div><!-- row -->
</div><!-- container -->
</section>

@include('front.includes.box-categories')

@endsection

@section('cssPage')
<link type="text/css" rel="stylesheet" href="/front/css/lightslider.css?1" />
@endsection
@section('jsPage')
<script src="/front/js/lightslider.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  $("#lightSlider").lightSlider({
   gallery: true,
   item: 1,
   loop: true,
   slideMargin: 0,
   thumbItem: 8,

   onBeforeStart: function (el) {},
   onSliderLoad: function (el) {},
   onBeforeSlide: function (el) {},
   onAfterSlide: function (el) {},
   onBeforeNextSlide: function (el) {},
   onBeforePrevSlide: function (el) {}
 });
});
</script>


@endsection