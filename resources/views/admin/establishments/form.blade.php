{!! csrf_field() !!}

{{ $slot }}

<!-- <div id="description" class="tab-pane active"> -->
    <div class="heading-block">
      <h4>Descrição do Estabelecimento</h4>
    </div>

  <div class="row">
    <div class="col-xs-12 col-md-4">
      <div class="form-group @if ($errors->has('name')) has-error @endif">
        <label for="name" class="form-label">Nome do Estabelecimento</label>
        <input type="text" class="form-control" id="name" name="name" value="{{ $establishment->name ?? old('name') }}" placeholder="Nome do Estabelecimento" autofocus>
        @if ($errors->has('name'))
        <span class="help-block">
          <strong>{{ $errors->first('name') }}</strong>
        </span>
        @endif
      </div><!-- form-group -->
    </div><!-- cols -->
    <div class="col-xs-12 col-md-4">
      <div class="form-group @if ($errors->has('proprietary')) has-error @endif">
        <label for="proprietary" class="form-label">Proprietário</label>
        <input type="text" class="form-control" id="proprietary" name="proprietary" value="{{ $establishment->proprietary ?? old('proprietary') }}" placeholder="Nome do Proprietário" >
        @if ($errors->has('proprietary'))
        <span class="help-block">
          <strong>{{ $errors->first('proprietary') }}</strong>
        </span>
        @endif
      </div><!-- form-group -->
    </div><!-- cols -->

    <div class="col-xs-12 col-md-4">
      <div class="form-group @if ($errors->has('category_id')) has-error @endif">
        <label for="category_id" class="form-label">Nome da Categoria</label>
        <select class="form-control" id="category_id" name="category_id">
          @if(isset($establishment->category_id))
          <optgroup label="Escolhido">
            <option value="{{ $establishment->category_id }}" selected>{{ $establishment->getPostCategory() }}</option>
          </optgroup>
          @endif

          <optgroup label="Escolha uma Opção">
            @foreach($categories as $category)
            <option value="{{ $category->id }}">{{ $category->title }}</option>
            @endforeach
          </optgroup>
        </select>

        @if ($errors->has('category_id'))
        <span class="help-block">
          <strong>{{ $errors->first('category_id') }}</strong>
        </span>
        @endif
      </div><!-- form-group -->
    </div><!-- cols -->

    <hr class="invisible">

    <div class="col-xs-12 col-md-12">
      <div class="form-group @if ($errors->has('slug')) has-error @endif">
        <label for="slug" class="form-label">URL Amigável</label>
        <input type="text" class="form-control" id="slug" name="slug" value="{{ $establishment->slug ?? old('slug') }}" readonly placeholder="URL Amigável" >
        @if ($errors->has('slug'))
        <span class="help-block">
          <strong>{{ $errors->first('slug') }}</strong>
        </span>
        @endif
      </div><!-- form-group -->
    </div><!-- cols -->
  </div><!-- row -->


  <div class="row">
    <div class="col-xs-12 col-md-12">
      <div class="form-group @if ($errors->has('description')) has-error @endif">
        <label for="description" class="form-label">Descrição ou serviços prestados</label>
        <textarea name="description" id="description" cols="30" rows="10" class="form-control summernote" placeholder="Descrição ou serviços prestados...">{{ $establishment->description ??  old('description') }}</textarea>
        @if ($errors->has('description'))
        <span class="help-block">
          <strong>{{ $errors->first('description') }}</strong>
        </span>
        @endif
      </div><!-- form-group -->
    </div><!-- cols -->
  </div><!-- row -->


  <div class="row">
    <div class="col-xs-12 col-md-2">
      <div class="form-group @if ($errors->has('cep')) has-error @endif">
        <label for="cep" class="form-label">CEP</label>
        <input type="text" class="form-control" id="cep" name="cep" value="{{ $establishment->cep ?? old('cep') }}" placeholder="Seu CEP" >
        @if ($errors->has('cep'))
        <span class="help-block">
          <strong>{{ $errors->first('cep') }}</strong>
        </span>
        @endif
      </div><!-- form-group -->
    </div><!-- cols -->
    <div class="col-xs-12 col-md-8">
      <div class="form-group @if ($errors->has('address')) has-error @endif">
        <label for="address" class="form-label">Endereço</label>
        <input type="text" class="form-control" id="address" name="address" value="{{ $establishment->address ?? old('address') }}" placeholder="Endereço" >
        @if ($errors->has('address'))
        <span class="help-block">
          <strong>{{ $errors->first('address') }}</strong>
        </span>
        @endif
      </div><!-- form-group -->
    </div><!-- cols -->
    <div class="col-xs-12 col-md-2">
      <div class="form-group @if ($errors->has('number')) has-error @endif">
        <label for="number" class="form-label">Número</label>
        <input type="text" class="form-control" id="number" name="number" value="{{ $establishment->number ?? old('number') }}" placeholder="Número" >
        @if ($errors->has('number'))
        <span class="help-block">
          <strong>{{ $errors->first('number') }}</strong>
        </span>
        @endif
      </div><!-- form-group -->
    </div><!-- cols -->
  </div><!-- row -->

  <div class="row">
    <div class="col-xs-12 col-md-3">
      <div class="form-group @if ($errors->has('distric')) has-error @endif">
        <label for="distric" class="form-label">Bairro</label>
        <input type="text" class="form-control" id="distric" name="distric" value="{{ $establishment->distric ?? old('distric') }}" placeholder="Bairro" >
        @if ($errors->has('distric'))
        <span class="help-block">
          <strong>{{ $errors->first('distric') }}</strong>
        </span>
        @endif
      </div><!-- form-group -->
    </div><!-- cols -->
    <div class="col-xs-12 col-md-3">
      <div class="form-group @if ($errors->has('city')) has-error @endif">
        <label for="city" class="form-label">Cidade</label>
        <input type="text" class="form-control" id="city" name="city" value="{{ $establishment->city ?? old('city') }}" placeholder="Cidade" >
        @if ($errors->has('city'))
        <span class="help-block">
          <strong>{{ $errors->first('city') }}</strong>
        </span>
        @endif
      </div><!-- form-group -->
    </div><!-- cols -->
    <div class="col-xs-12 col-md-3">
      <div class="form-group @if ($errors->has('state')) has-error @endif">
        <label for="state" class="form-label">Estado</label>
        <input type="text" class="form-control" id="state" name="state" value="{{ $establishment->state ?? old('state') }}" placeholder="Estado" >
        @if ($errors->has('state'))
        <span class="help-block">
          <strong>{{ $errors->first('state') }}</strong>
        </span>
        @endif
      </div><!-- form-group -->
    </div><!-- cols -->
    <div class="col-xs-12 col-md-3">
      <div class="form-group @if ($errors->has('country')) has-error @endif">
        <label for="country" class="form-label">País</label>
        <input type="text" class="form-control" id="country" name="country" value="Brasil" readonly placeholder="País" >
        @if ($errors->has('country'))
        <span class="help-block">
          <strong>{{ $errors->first('country') }}</strong>
        </span>
        @endif
      </div><!-- form-group -->
    </div><!-- cols -->
  </div><!-- row -->

  <div class="row">
    <div class="col-xs-12 col-md-6">
      <div class="form-group @if ($errors->has('email')) has-error @endif">
        <label for="email" class="form-label">E-mail de Contato</label>
        <input type="email" class="form-control" id="email" name="email" value="{{ $establishment->email ?? old('email') }}" placeholder="E-mail de Contato" >
        @if ($errors->has('email'))
        <span class="help-block">
          <strong>{{ $errors->first('email') }}</strong>
        </span>
        @endif
      </div><!-- form-group -->
    </div><!-- col-md-6 -->

    <div class="col-xs-12 col-md-6">
      <div class="form-group @if ($errors->has('website')) has-error @endif">
        <label for="website" class="form-label">Link do Site</label>
        <input type="text" class="form-control" id="website" name="website" value="{{ $establishment->website ?? old('website') }}" placeholder="Link do Site" >
        @if ($errors->has('website'))
        <span class="help-block">
          <strong>{{ $errors->first('website') }}</strong>
        </span>
        @endif
      </div><!-- form-group -->
    </div><!-- col-md-6 -->
  </div><!-- row -->


  <div class="row">
    <div class="col-xs-12 col-md-4">
      <div class="form-group @if ($errors->has('phone')) has-error @endif">
        <label for="phone" class="form-label">Telefone (DDD)</label>
        <input type="text" class="form-control" id="phone" name="phone" value="{{ $establishment->phone ?? old('phone') }}" placeholder="Telefone (DDD)" >
        @if ($errors->has('phone'))
        <span class="help-block">
          <strong>{{ $errors->first('phone') }}</strong>
        </span>
        @endif

        <div class="checkbox2">
            <label>
              @if(isset($establishment->phone_whatsapp))
              @if($establishment->phone_whatsapp == 1)
              <input type="checkbox" name="phone_whatsapp" id="phone_whatsapp" value="1" checked>
              @else
              <input type="checkbox" name="phone_whatsapp" id="phone_whatsapp" value="0">
              @endif
              @else
              <input type="checkbox" name="phone_whatsapp" id="phone_whatsapp" value="1">
              @endif
              Telefone é WhatsApp?
            </label>
          </div>
      </div><!-- form-group -->
    </div><!-- cols -->
    <div class="col-xs-12 col-md-4">
      <div class="form-group @if ($errors->has('cellphone')) has-error @endif">
        <label for="cellphone" class="form-label">Celular (DDD)</label>
        <input type="text" class="form-control" id="cellphone" name="cellphone" value="{{ $establishment->cellphone ?? old('cellphone') }}" placeholder="Celular (DDD)" >
        @if ($errors->has('cellphone'))
        <span class="help-block">
          <strong>{{ $errors->first('cellphone') }}</strong>
        </span>
        @endif
      </div><!-- form-group -->
    </div><!-- cols -->
    <div class="col-xs-12 col-md-4">

      <div class="form-group @if ($errors->has('status')) has-error @endif">
        <label for="status" class="form-label">Status do Estabelecimento</label>
        <select class="form-control" id="status" name="status">
          @if(isset($establishment->status))
          @if($establishment->status == 0 )
          <option value="0" selected>Desabilitado</option> 
          <option value="1">Habilitar</option>
          @else
          <option value="1" selected>Habilitado</option>
          <option value="0">Desabilitar</option> 
          @endif
          @else
          <option value="1" selected>Habilitado</option>
          <option value="0">Desabilitado</option> 
          @endif
        </select>

        @if ($errors->has('status'))
        <span class="help-block">
          <strong>{{ $errors->first('status') }}</strong>
        </span>
        @endif
      </div><!-- form-group -->
    </div><!-- cols -->
  </div><!-- row -->
  <!-- </div> < !-- tab-pane -->

  <hr>

  <div class="row">
    <div class="col-md-6">
      <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i>Salvar</button>
      <a href="{{ route('establishments.index') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i>Cancelar</a>
    </div><!-- col-md-6 -->
  </div>