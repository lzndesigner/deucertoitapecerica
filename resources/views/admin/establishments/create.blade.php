@extends('admin.base')
@section('title', 'Novo Estabelecimento')

@section('content')
<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title')</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li><a href="{{ route('establishments.index') }}">Página de Estabelecimentos</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End Page Header -->


<!-- START CONTAINER -->
<div class="container-default">

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            @yield('title')
          </div>

          <div class="panel-body">

            <form action="{{ route('establishments.store')}}" enctype="multipart/form-data" method="POST">
              <!--
              <div class="tabs">
                <ul class="nav nav-tabs">
                  <li class="active">
                    <a href="#description" data-toggle="tab"><i class="fa fa-star"></i> Descrição</a>
                  </li>
                  <li class="">
                    <a href="#images" data-toggle="tab">Imagens</a>
                  </li>
                </ul>
                <div class="tab-content">
                -->
                  @component('admin.establishments.form')
                  <!-- <div id="images" class="tab-pane"> -->
                    <div class="row">
                      <div class="col-md-12">
                        <div class="heading-block">
                          <h4>Imagens do Estabelecimento</h4>
                        </div>
                      </div>
                      
                      <div class="col-md-12">
                        <input type="file" style="display: none;" name="photos[]" id="input-add-photos" accept="image/jpg, image/jpeg, image/png" multiple>
                        <button class="btn btn-success" id="add-photos-to-establishment" type="button">
                          <i class="fa fa-plus"></i> Selecionar Imagens
                        </button>
                        <span class="btn disabled" id="display-add-photos">0 selecionados</span>
                      </div>

                      <div class="col-md-12">
                        <span class="help-block">É obrigatório o envio de 1 imagem para Capa do Estabelecimento</span>
                        <hr>
                      </div>
                    </div>
                  </div>
                  @endcomponent
             <!--    </div>tab-content
                           </div>tabs -->
            </form>

          </div><!-- panel-body -->


        </div><!-- panel-default -->
      </div><!-- col-md-12 -->


    </div><!-- row -->
  </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')
<link href="{{ asset('front/js/summernote/summernote.css?13') }}" rel="stylesheet">
<link href="{{ asset('front/js/summernote/summernote-bs4.css?13') }}" rel="stylesheet">
@endsection

@section('jsPage')
<!-- ================================================
Bootstrap WYSIHTML5
================================================ -->
<!-- main file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js"></script>
<!-- bootstrap file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

<script type="text/javascript" src="/front/js/summernote/summernote.js?2"></script>
<script type="text/javascript" src="/front/js/summernote/lang/summernote-pt-BR.js?2"></script>

<script type="text/javascript">
  /* SUMMERNOTE*/
  $(document).ready(function() {
    $('.summernote').summernote({
      lang: 'pt-BR'
    });
    $('button[data-event="showImageDialog"], button[data-event="showVideoDialog"], button[data-event="codeview"], .note-toolbar .note-table').hide();
  });
</script>

<script>

  /* Adicionar arquivos */
  $('#add-photos-to-establishment').click(function(e) {
    e.preventDefault();
    $("#input-add-photos").click();
  });

  $("#input-add-photos").change(function(e) {
    let display = $('span#display-add-photos'),
    files = e.target.files,
    count = files.length;

    if(count > 0) {
      display.empty().html(count + ' selecionados.')
    }else{
      display.empty().html('0 selecionados.')
    }
  });

  /* BOOTSTRAP WYSIHTML5 */
  $('.textarea').wysihtml5();
</script>

<!-- Query String ToSlug - Transforma o titulo em URL amigavel sem acentos ou espaço -->
<script type="text/javascript" src="/backend/js/jquery.stringToSlug.min.js"></script>
<script type="text/javascript">
  $('input[name="name"]').stringToSlug({
    setEvents: 'keyup keydown blur',
    getPut: 'input[name="slug"]',
    space: '-',
    replace: '/\s?\([^\)]*\)/gi',
    AND: 'e'
  });
</script>
@endsection