<div class="sidebar clearfix">

    <ul class="sidebar-panel nav">
        <li class="sidetitle">NAVEGAÇÃO</li>
        <li><a href="{{ url('/') }}" target="_Blank"><span class="icon color5"><i class="fa fa-desktop"></i></span>Meu Site</a></li>
        <li><a href="{{ url('/dashboard') }}"><span class="icon color5"><i class="fa fa-home"></i></span>Início</a></li>
        <li><a href="http://webmail.innsystem.com.br" target="_Blank"><span class="icon color5"><i class="fa fa-comments"></i></span>Web Mail</a></li>

        <li><a href="#"><span class="icon color7"><i class="fa fa-cogs"></i></span> Configuração <span class="caret"></span></a>
            <ul>
                <li><a href="{{ route('configs.index') }}"><i class="fa fa-cog"></i>Site em Geral</a></li>
                <li><a href="{{ route('users.index') }}"><i class="fa fa-user"></i>Usuários Administrativos</a></li>
            </ul>
        </li>   

        <li><a href="#"><span class="icon color7"><i class="fa fa-bank"></i></span> Catálogo <span class="caret"></span></a>
            <ul>
                <li><a href="{{ route('categories.index') }}"><i class="fa fa-bars"></i> Categorias</a></li>
                <li><a href="{{ route('establishments.index') }}"><i class="fa fa-heart"></i> Estabelecimentos</a></li>
            </ul>
        </li>


        <li><a href="#"><span class="icon color7"><i class="fa fa-book"></i></span> Páginas <span class="caret"></span></a>
            <ul>
                <li><a href="{{ route('about.index') }}"><i class="fa fa-star"></i> Sobre</a></li>
                <li><a href="{{ route('calendars.index') }}"><i class="fa fa-calendar"></i> Calendário</a></li>
                <li><a href="{{ route('blogs.index') }}"><i class="fa fa-comments"></i> Blog</a></li>
                <li><a href="{{ route('sliders.index') }}"><i class="fa fa-image"></i> Sliders</a></li>
                @if(Auth::User()->level === 'developer')
                <li><a href="{{ route('pages.index') }}"><i class="fa fa-book"></i> Página de Informações</a></li>
                @endif
            </ul>
        </li>
    </ul>

    <ul class="sidebar-panel nav">
        <li class="sidetitle">OUTROS</li>
        <li><a href="{{ url('https://www.innsystem.com.br/central-ajuda') }}" target="_Blank"><span class="icon color15"><i class="fa fa-bank"></i></span>Central de Ajuda</a></li>
        <li><a href="{{ url('https://www.innsystem.com.br/contato') }}" target="_Blank"><span class="icon color15"><i class="fa fa-tags"></i></span>Ticket de Suporte</a></li>
    </ul>

</div><!-- sidebar -->
