@extends('admin.base')
@section('title', 'Detalhes do Depoimento')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title') - {{ $review->name }}</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li><a href="{{ route('reviews.index') }}">Página de Depoimentos</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End review Header -->


<!-- START CONTAINER -->
<div class="container-default">

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            <h2>{{ $review->name }}</h2>
          </div>

          <div class="panel-body">
            <blockquote>{!! $review->body !!}</blockquote>
            <p><small>Criado em {{ $review->created_at->format('d/m/Y H:i:s') }} | Atualizado em {{ $review->updated_at->format('d/m/Y H:i:s') }}</small></p>

            <hr>

            <a href="{{ route('reviews.index') }}" class="btn btn-xs btn-warning">Voltar</a>
            <a href="{{ route('reviews.edit', $review->id) }}" class="btn btn-xs btn-primary">Editar</a>
            <a href="{{ route('reviews.destroy', $review->id) }}" class="btn btn-xs btn-danger" onclick="event.preventDefault(); document.getElementById('delete-form-reviews').submit();"><i class="fa fa-remove"></i> Deletar</a></li>
            <form id="delete-form-reviews" action="{{ route('reviews.destroy', $review->id) }}" method="POST" style="display: none;">
             {!! csrf_field() !!}
             <input type="hidden" name="_method" value="DELETE">
           </form>

         </div><!-- panel-body -->


       </div><!-- panel-default -->
     </div><!-- col-md-12 -->


   </div><!-- row -->
 </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')
@endsection

@section('jsPage')
@endsection