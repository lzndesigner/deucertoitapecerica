@extends('admin.base')
@section('title', 'Editar O Sobre')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title')</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End Page Header -->


<!-- START CONTAINER -->
<div class="container-default">

  @include('elements.messages')

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
           @yield('title')
         </div>

         <div class="panel-body">

          <form action="{{ route('about.store')}}" enctype="multipart/form-data" method="POST">
            {!! csrf_field() !!}

            <div class="row">
              <div class="col-xs-12 col-md-12">

                <div class="form-group @if ($errors->has('page_description')) has-error @endif">
                  <label for="page_description" class="form-label">Descrição da Página</label>
                  <textarea rows="18" class="form-control summernote" id="page_description" name="page_description" value="{{ $about->page_description ?? old('page_description') }}" placeholder="Descrição da Página" autofocus>{{ $about->page_description ?? old('page_description') }}</textarea>
                  @if ($errors->has('page_description'))
                  <span class="help-block">
                    <strong>{{ $errors->first('page_description') }}</strong>
                  </span>
                  @endif
                </div><!-- form-group -->

              </div>
            </div>            

          </div><!-- col-md-11 -->

          <div class="row">
            <div class="col-md-6">
              <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i>Salvar</button>
              <a href="{{ url('/dashboard') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> Cancelar</a>
            </div><!-- col-md-6 -->
          </div>

        </form>

      </div><!-- panel-body -->


    </div><!-- panel-default -->
  </div><!-- col-md-12 -->


</div><!-- row -->
</div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')
<link rel="stylesheet" href="/bower_components/blueimp-file-upload/css/jquery.fileupload.css">
@endsection

@section('jsPage')
<!-- ================================================
Bootstrap WYSIHTML5
================================================ -->
<!-- main file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js"></script>
<!-- bootstrap file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

<!-- ================================================
Summernote
================================================ -->
<script type="text/javascript" src="/backend/js/summernote/summernote.min.js?1"></script>

<script>
  /* BOOTSTRAP WYSIHTML5 */
  $('.textarea').wysihtml5();

  /* SUMMERNOTE*/
  $(document).ready(function() {
    $('.summernote').summernote();
  });
</script>
@endsection