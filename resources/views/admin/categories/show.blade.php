@extends('admin.base')
@section('title', 'Detalhes da Categoria')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title') - {{ $category->title }}</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li><a href="{{ route('categories.index') }}">Página de Categorias</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End category Header -->


<!-- START CONTAINER -->
<div class="container-default">

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            <h2>{{ $category->title }}</h2>
          </div>

          <div class="panel-body">

            <p><small>Criado em {{ $category->created_at->format('d/m/Y H:i:s') }} | Atualizado em {{ $category->updated_at->format('d/m/Y H:i:s') }}</small></p>
            
            <hr>

            <a href="{{ route('categories.index') }}" class="btn btn-xs btn-warning">Voltar</a>
            <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-xs btn-primary">Editar</a>
            <a href="{{ route('categories.destroy', $category->id) }}" class="btn btn-xs btn-danger" onclick="event.preventDefault(); document.getElementById('delete-form-categories').submit();"><i class="fa fa-remove"></i> Deletar</a></li>
            <form id="delete-form-categories" action="{{ route('categories.destroy', $category->id) }}" method="POST" style="display: none;">
             {!! csrf_field() !!}
             <input type="hidden" name="_method" value="DELETE">
           </form>

         </div><!-- panel-body -->


       </div><!-- panel-default -->
     </div><!-- col-md-12 -->


   </div><!-- row -->
 </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')
@endsection

@section('jsPage')
@endsection