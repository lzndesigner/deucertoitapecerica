@extends('admin.base')
@section('title', 'Editar Categoria')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title') - {{ $category->title }}</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li><a href="{{ route('categories.index') }}">Página de Categorias</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End category Header -->


<!-- START CONTAINER -->
<div class="container-default">

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
           @yield('title') - {{ $category->title }}
         </div>

         <div class="panel-body">

          <form action="{{ route('categories.update', $category->id)}}" enctype="multipart/form-data" method="POST">
            <input type="hidden" name="_method" value="PUT">
            @component('admin.categories.form')
                <div id="images" class="tab-pane">

                  <div class="row">
                    <div class="col-md-12">
                      <input type="file" style="display: none;" name="addPhotos[]" id="input-add-photos" multiple>
                      <button class="btn btn-success" id="add-photos-to-category" type="button">
                        <i class="fa fa-plus"></i> Adicionar Imagens
                      </button>
                      <span class="btn disabled" id="display-add-photos">0 selecionados</span>
                    </div>
                  </div>

                  <hr>

                  @slot('category', $category)
                  <div class="row">
                    @forelse($category->photos as $photo)
                    <div id="photo-{{ $photo->id }}" class="col-xs-6 col-sm-4 col-md-3">

                      <div class="panel panel-default">
                        <div class="panel-body">
                          <img id="photo-preview-{{ $photo->id }}" src="{{ asset('storage/' . $photo->filename) }}" alt="{{ $category->slug }}" class="img-thumbnail img-responsive" alt="#{{ $photo->id}}">
                        </div>
                        <div class="panel-footer">
                          <div class="btn-group btn-group-xs">
                            <input type="file" name="photos[{{ $photo->id }}]" id="{{ $photo->id }}" class="update-photo" style="display: none;"/>
                            <button class="btn btn-block btn-info" type="button" onclick="document.getElementById({{ $photo->id }}).click()">Alterar imagem</button>
                            <button class="btn btn-block btn-danger" type="button" onclick="remove({{ $category->id }}, {{ $photo->id }})">Remover</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    @empty
                    <p>Nao existem fotos para essa Categoria.</p>
                    @endforelse
                  </div>                
                </div><!-- tab-images -->
                @endcomponent
          </form>

        </div><!-- panel-body -->


      </div><!-- panel-default -->
    </div><!-- col-md-12 -->


  </div><!-- row -->
</div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')
@endsection

@section('jsPage')
<!-- ================================================
Bootstrap WYSIHTML5
================================================ -->
<!-- main file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js"></script>
<!-- bootstrap file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

<!-- ================================================
Summernote
================================================ -->
<script type="text/javascript" src="/backend/js/summernote/summernote.min.js?1"></script>

<script>

  function remove(category, photo) {
    let doc = $('#photo-' + photo);
    let request = $.ajax({
     url: '/dashboard/categorys/' + category + '/photos/' + photo,
     type: 'DELETE',
     statusCode: {
       204: function() {
            doc.fadeOut(); // ocultar registro
          }
        }
      })
  }

  /* Gerar Previa */
  $('.update-photo').change(function(e) {
    console.log('updated.')
    const ref = e.target;

    if(!ref.files) return;

    let id = ref.id,
    image = $('#photo-preview-' + id),
    file = ref.files[0],
    old = image.attr('src')

    var reader = new FileReader();

    reader.onload = function(event) {

      image.css({filter: blur(1.5)})

      image.fadeOut(500, function() {
        image.attr('src', event.target.result);
        image.fadeIn(200, function() {
          image.css({filter: blur(0)})
        });
      })
    }
      //
      reader.readAsDataURL(file);
    });

  /* Adicionar arquivos */
  $('#add-photos-to-category').click(function(e) {
    e.preventDefault();
    $("#input-add-photos").click();
  });

  $("#input-add-photos").change(function(e) {
    let display = $('span#display-add-photos'),
    files = e.target.files,
    count = files.length;

    if(count > 0) {
      display.empty().html(count + ' selecionados.')
    }else{
      display.empty().html('0 selecionados.')
    }
  });


  /* BOOTSTRAP WYSIHTML5 */
  $('.textarea').wysihtml5();

  /* SUMMERNOTE*/
  $(document).ready(function() {
    $('.summernote').summernote();
  });
</script>

<!-- Query String ToSlug - Transforma o titulo em URL amigavel sem acentos ou espaço -->
<script type="text/javascript" src="/backend/js/jquery.stringToSlug.min.js"></script>
<script type="text/javascript">
  $('input[name="title"]').stringToSlug({
    setEvents: 'keyup keydown blur',
    getPut: 'input[name="slug"]',
    space: '-',
    replace: '/\s?\([^\)]*\)/gi',
    AND: 'e'
  });
</script>
@endsection