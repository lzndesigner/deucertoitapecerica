{!! csrf_field() !!}


{{ $slot }}

<div class="form-group @if ($errors->has('title')) has-error @endif">
  <label for="title" class="form-label">Nome da Categoria</label>
  <input type="text" class="form-control" id="title" name="title" value="{{ $category->title ?? old('title') }}" placeholder="Nome da Categoria" autofocus>
  @if ($errors->has('title'))
  <span class="help-block">
    <strong>{{ $errors->first('title') }}</strong>
  </span>
  @endif
</div><!-- form-group -->


<div class="form-group @if ($errors->has('slug')) has-error @endif">
  <label for="slug" class="form-label">URL Amigável</label>
  <input type="text" class="form-control" id="slug" name="slug" readonly value="{{ $category->slug ?? old('slug') }}" placeholder="URL Amigável">
  <span class="help-block">* É preenchido automaticamente.</span>
  @if ($errors->has('slug'))
  <span class="help-block">
    <strong>{{ $errors->first('slug') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="form-group @if ($errors->has('status')) has-error @endif">
  <label for="status" class="form-label">Status do Cliente</label>
  <select class="form-control" id="status" name="status">
    @if(isset($category->status))
    @if($category->status == 0 )
    <option value="0" selected>Desabilitado</option> 
    <option value="1">Habilitar</option>
    @else
    <option value="0">Desabilitar</option> 
    <option value="1" selected>Habilitado</option>
    @endif
    @else
    <option value="1" selected>Habilitado</option>
    <option value="0">Desabilitado</option> 
    @endif
  </select>

  @if ($errors->has('status'))
  <span class="help-block">
    <strong>{{ $errors->first('status') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="row">
  <div class="col-md-6">
    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i>Salvar</button>
    <a href="{{ route('categories.index') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i>Cancelar</a>
  </div><!-- col-md-6 -->
</div>