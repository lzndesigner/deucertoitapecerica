@extends('admin.base')
@section('title', 'Lista de Slider')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title')</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard/') }}">Inicio</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End Page Header -->


<!-- START CONTAINER -->
<div class="container-default">

  @include('elements.messages')

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            @yield('title')
          </div>

          <div class="panel-body">
            <div class="table-responsive">
             @if(count($sliders) > 0)
             <table class="table table-hover table-bordered table-striped">
              <thead>
                <tr>
                  <td width="5%"><a href="{{ Request::fullUrlWithQuery(['sort' => 'id']) }}">#</a></td>
                  <td width="6%">Imagem</td>
                  <td><a href="{{ Request::fullUrlWithQuery(['sort' => 'title']) }}">Title</a></td>
                  <td width="14%"><a href="{{ Request::fullUrlWithQuery(['sort' => 'created_at']) }}">Criação</a></td>
                  <td width="15%" class="text-right">Ações</td>
                </tr>
              </thead>
              <tbody>
                @foreach($sliders as $slider)
                <tr>
                  <td>
                    <div class="checkbox">
                      <input id="checkbox{{ $slider->id }}" type="checkbox">
                      <label for="checkbox{{ $slider->id }}">
                        <b>{{ $slider->id }}</b>
                      </label>
                    </div>
                  </td>
                  <td><img src="/storage/{{ $slider->image ?? old('image') }}" alt="{{ $slider->title }}" class="img-thumbnail"> </td>
                  <td>{{ $slider->title }}</td>
                  <td>{{ \Carbon\Carbon::parse($slider->created_at)->format('d/m/y H:i')  }}</td>
                  <td class="text-right">
                    <a href="{{ route('sliders.edit', $slider->id) }}" class="btn btn-xs btn-primary btn-icon" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-edit"></i></a>
                    <a href="{{ route('sliders.destroy', $slider->id) }}" class="btn btn-xs btn-danger btn-icon" onclick="event.preventDefault(); document.getElementById('delete-form-sliders-list').submit();" data-toggle="tooltip" data-original-title="Remover"><i class="fa fa-remove"></i></a></li>
                    <form id="delete-form-sliders-list" action="{{ route('sliders.destroy', $slider->id) }}" method="POST" style="display: none;">
                     {!! csrf_field() !!}
                     <input type="hidden" name="_method" value="DELETE">
                   </form>
                 </td>
               </tr>
               @endforeach

             </tbody>
           </table>
           @else
           <div class="alert alert-info">Nenhum slider cadastrado.</div>
           @endif

           <hr>

         </div><!-- table-responsive -->
       </div><!-- panel-body -->

       <div class="row">
        <div class="col-md-6">
          <a href="{{ route('sliders.create') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>Novo</a>
          <a href="#" class="btn btn-sm btn-danger"><i class="fa fa-remove"></i>Remover selecionados</a>
        </div><!-- col-md-6 -->

        <div class="col-md-6 text-right">
          {{ $sliders->links() }}
        </div><!-- col-md-6 -->
      </div>
      

    </div><!-- panel-default -->
  </div><!-- col-md-12 -->
  

</div><!-- row -->
</div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection


@section('cssPage')
@endsection

@section('jsPage')
@endsection