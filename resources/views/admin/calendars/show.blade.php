@extends('admin.base')
@section('title', 'Detalhes do Evento')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title') - {{ $calendar->name }}</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li><a href="{{ route('calendars.index') }}">Lista de Eventos</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End calendar Header -->


<!-- START CONTAINER -->
<div class="container-default">

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            <h2>{{ $calendar->name }}</h2>
            <h4>{{ $calendar->day }}/{{ $calendar->month }} às {{ $calendar->hour }}</h4>
          </div>

          <div class="panel-body">

            <img src="/storage/{{ $calendar->image }}" alt="Calendário" class="img-thumbnail">

            <p><small>Criado em {{ $calendar->created_at->format('d/m/Y H:i:s') }} | Atualizado em {{ $calendar->updated_at->format('d/m/Y H:i:s') }}</small></p>
            <blockquote>
              Endereço: {{ $calendar->local }}
              <br>
              Classificação: {{ $calendar->classification }}
              <br>
              {!! $calendar->description !!}</blockquote>

            <hr>

            <a href="{{ route('calendars.index') }}" class="btn btn-xs btn-warning">Voltar</a>
            <a href="{{ route('calendars.edit', $calendar->id) }}" class="btn btn-xs btn-primary">Editar</a>
            <a href="{{ route('calendars.destroy', $calendar->id) }}" class="btn btn-xs btn-danger" onclick="event.preventDefault(); document.getElementById('delete-form-calendars').submit();"><i class="fa fa-remove"></i> Deletar</a></li>
            <form id="delete-form-calendars" action="{{ route('calendars.destroy', $calendar->id) }}" method="POST" style="display: none;">
             {!! csrf_field() !!}
             <input type="hidden" name="_method" value="DELETE">
           </form>

         </div><!-- panel-body -->


       </div><!-- panel-default -->
     </div><!-- col-md-12 -->


   </div><!-- row -->
 </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')
@endsection

@section('jsPage')
@endsection