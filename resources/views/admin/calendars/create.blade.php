@extends('admin.base')
@section('title', 'Novo Evento')

@section('content')
<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title')</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li><a href="{{ route('calendars.index') }}">Lista de Eventos</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End Page Header -->


<!-- START CONTAINER -->
<div class="container-default">

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            @yield('title')
          </div>

          <div class="panel-body">

            <form action="{{ route('calendars.store')}}" enctype="multipart/form-data" method="POST">
              <!-- <div class="tabs">
                <ul class="nav nav-tabs">
                  <li class="active">
                    <a href="#description" data-toggle="tab"><i class="fa fa-star"></i> Descrição</a>
                  </li>
                  <li class="">
                    <a href="#images" data-toggle="tab">Imagens</a>
                  </li>
                </ul>
                <div class="tab-content"> -->
                 @component('admin.calendars.form')
                 <!-- <div id="images" class="tab-pane"> -->
                   <div class="row">
                    <div class="col-md-12">
                        <div class="heading-block">
                          <h4>Imagens do Evento</h4>
                        </div>
                      </div>

                    <div class="col-md-12">
                      <input type="file" style="display: none;" name="photos[]" id="input-add-photos" multiple>
                      <button class="btn btn-success" id="add-photos-to-calendar" type="button">
                        <i class="fa fa-plus"></i> Selecionar Imagens
                      </button>
                      <span class="btn disabled" id="display-add-photos">0 selecionados</span>
                    </div>

                    <div class="col-md-12">
                        <span class="help-block">É obrigatório o envio de 1 imagem para Capa do Evento</span>
                        <hr>
                      </div>
                  </div>
                </div><!-- tab-image -->
                @endcomponent
              </div><!-- tab-content -->
            </div><!-- tabs -->
          </form>

        </div><!-- panel-body -->


      </div><!-- panel-default -->
    </div><!-- col-md-12 -->


  </div><!-- row -->
</div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')
@endsection

@section('jsPage')
<!-- ================================================
Bootstrap WYSIHTML5
================================================ -->
<!-- main file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js"></script>
<!-- bootstrap file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

<!-- ================================================
Summernote
================================================ -->
<script type="text/javascript" src="/backend/js/summernote/summernote.min.js?1"></script>

<script>
  /* Adicionar arquivos */
  $('#add-photos-to-calendar').click(function(e) {
    e.preventDefault();
    $("#input-add-photos").click();
  });

  $("#input-add-photos").change(function(e) {
    let display = $('span#display-add-photos'),
    files = e.target.files,
    count = files.length;

    if(count > 0) {
      display.empty().html(count + ' selecionados.')
    }else{
      display.empty().html('0 selecionados.')
    }
  });


  /* BOOTSTRAP WYSIHTML5 */
  $('.textarea').wysihtml5();

  /* SUMMERNOTE*/
  $(document).ready(function() {
    $('.summernote').summernote();
  });
</script>

<!-- Query String ToSlug - Transforma o titulo em URL amigavel sem acentos ou espaço -->
<script type="text/javascript" src="/backend/js/jquery.stringToSlug.min.js"></script>
<script type="text/javascript">
  $('input[name="name"]').stringToSlug({
    setEvents: 'keyup keydown blur',
    getPut: 'input[name="slug"]',
    space: '-',
    replace: '/\s?\([^\)]*\)/gi',
    AND: 'e'
  });
</script>
@endsection