{!! csrf_field() !!}

{{ $slot }}

<!-- <div id="description" class="tab-pane active"> -->
    <div class="heading-block">
      <h4>Descrição do Evento</h4>
    </div>

  <div class="form-group @if ($errors->has('name')) has-error @endif">
    <label for="name" class="form-label">Nome do Evento</label>
    <input type="text" class="form-control" id="name" name="name" value="{{ $calendar->name ?? old('name') }}" placeholder="Nome do Evento" autofocus>
    @if ($errors->has('name'))
    <span class="help-block">
      <strong>{{ $errors->first('name') }}</strong>
    </span>
    @endif
  </div><!-- form-group -->

  <div class="form-group @if ($errors->has('slug')) has-error @endif">
    <label for="slug" class="form-label">URL Amigável</label>
    <input type="text" class="form-control" id="slug" name="slug" readonly value="{{ $calendar->slug ?? old('slug') }}" placeholder="URL Amigável">
    <span class="help-block">* É preenchido automaticamente.</span>
    @if ($errors->has('slug'))
    <span class="help-block">
      <strong>{{ $errors->first('slug') }}</strong>
    </span>
    @endif
  </div><!-- form-group -->



  <div class="row">
    <div class="col-xs-12 col-md-3">
      <div class="form-group @if ($errors->has('day')) has-error @endif">
        <label for="day" class="form-label">Dia</label>
        <select class="form-control" id="day" name="day">
          @if(isset($calendar->day))
          <optgroup label="Escolhido">
            <option value="{{ $calendar->day }}" selected>{{ $calendar->day }}</option>
          </optgroup>
          @endif
          <optgroup label="Escolha uma Opção">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
            <option value="13">13</option>
            <option value="14">14</option>
            <option value="15">15</option>
            <option value="16">16</option>
            <option value="17">17</option>
            <option value="18">18</option>
            <option value="19">19</option>
            <option value="20">20</option>
            <option value="21">21</option>
            <option value="22">22</option>
            <option value="23">23</option>
            <option value="24">24</option>
            <option value="25">25</option>
            <option value="26">26</option>
            <option value="27">27</option>
            <option value="28">28</option>
            <option value="29">29</option>
            <option value="30">30</option>
            <option value="31">31</option>
          </optgroup>
        </select>

        @if ($errors->has('day'))
        <span class="help-block">
          <strong>{{ $errors->first('day') }}</strong>
        </span>
        @endif
      </div><!-- form-group -->
    </div><!-- cols -->
    <div class="col-xs-12 col-md-3">
      <div class="form-group @if ($errors->has('month')) has-error @endif">
        <label for="month" class="form-label">Mês</label>
        <select class="form-control" id="month" name="month">
          @if(isset($calendar->month))
          <optgroup label="Escolhido">
            <option value="{{ $calendar->month }}" selected>{{ $calendar->month }}</option>          
          </optgroup>
          @endif
          <optgroup label="Escolha uma Opção">
            <option value="1">Jan</option>
            <option value="2">Fev</option>
            <option value="3">Mar</option>
            <option value="4">Abr</option>
            <option value="5">Mai</option>
            <option value="6">Jun</option>
            <option value="7">Jul</option>
            <option value="8">Ago</option>
            <option value="9">Set</option>
            <option value="10">Out</option>
            <option value="11">Nov</option>
            <option value="12">Dez</option>
          </optgroup>
        </select>

        @if ($errors->has('month'))
        <span class="help-block">
          <strong>{{ $errors->first('month') }}</strong>
        </span>
        @endif
      </div><!-- form-group -->
    </div><!-- cols -->
    <div class="col-xs-12 col-md-3">
      <div class="form-group @if ($errors->has('year')) has-error @endif">
        <label for="year" class="form-label">Ano</label>
        <select class="form-control" id="year" name="year">
          @if(isset($calendar->year))
          <optgroup label="Escolhido">
            <option value="{{ $calendar->year }}" selected>{{ $calendar->year }}</option>          
          </optgroup>
          @endif
          <optgroup label="Escolha uma Opção">
            <option value="2022">2022</option>
            <option value="2021">2021</option>
            <option value="2020">2020</option>
            <option value="2019">2019</option>
            <option value="2018">2018</option>
          </optgroup>
        </select>

        @if ($errors->has('year'))
        <span class="help-block">
          <strong>{{ $errors->first('year') }}</strong>
        </span>
        @endif
      </div><!-- form-group -->
    </div><!-- cols -->
    <div class="col-xs-12 col-md-3">
      <div class="form-group @if ($errors->has('hour')) has-error @endif">
        <label for="hour" class="form-label">Hora</label>
        <input type="text" class="form-control" id="hour" name="hour" value="{{ $calendar->hour ?? old('hour') }}" placeholder="Hora de Realização">
        @if ($errors->has('hour'))
        <span class="help-block">
          <strong>{{ $errors->first('hour') }}</strong>
        </span>
        @endif
      </div><!-- form-group -->
    </div><!-- cols -->
  </div><!-- row -->


  <div class="form-group @if ($errors->has('local')) has-error @endif">
    <label for="local" class="form-label">Local de Realização</label>
    <input type="text" class="form-control" id="local" name="local" value="{{ $calendar->local ?? old('local') }}" placeholder="Local de Realização">
    @if ($errors->has('local'))
    <span class="help-block">
      <strong>{{ $errors->first('local') }}</strong>
    </span>
    @endif
  </div><!-- form-group -->

  <div class="form-group @if ($errors->has('classification')) has-error @endif">
    <label for="classification" class="form-label">Classificação</label>
    <select class="form-control" id="classification" name="classification">
      @if(isset($calendar->classification))
      <optgroup label="Escolhido">
        <option value="{{ $calendar->classification }}" selected>{{ $calendar->classification }}</option>
      </optgroup>
      @endif
      <optgroup label="Escolha uma Opção">
        <option value="livre">Livre</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
        <option value="16">16</option>
        <option value="17">17</option>
        <option value="18">18</option>
      </optgroup>
    </select>

    @if ($errors->has('classification'))
    <span class="help-block">
      <strong>{{ $errors->first('classification') }}</strong>
    </span>
    @endif
  </div><!-- form-group -->





  <div class="form-group @if ($errors->has('description')) has-error @endif">
    <label for="description" class="form-label">Descrição do Evento</label>
    <textarea name="description" id="description" cols="30" rows="10" class="form-control summernote" placeholder="Descrição...">{{ $calendar->description ??  old('description') }}</textarea>
    @if ($errors->has('description'))
    <span class="help-block">
      <strong>{{ $errors->first('description') }}</strong>
    </span>
    @endif
  </div><!-- form-group -->



  <div class="form-group @if ($errors->has('status')) has-error @endif">
    <label for="status" class="form-label">Status do Evento</label>
    <select class="form-control" id="status" name="status">
      @if(isset($client->status))
      @if($client->status == 0 )
      <option value="0" selected>Desabilitado</option> 
      <option value="1">Habilitar</option>
      @else
      <option value="0">Desabilitar</option> 
      <option value="1" selected>Habilitado</option>
      @endif
      @else
      <option value="1" selected>Habilitado</option>
      <option value="0">Desabilitado</option> 
      @endif
    </select>

    @if ($errors->has('status'))
    <span class="help-block">
      <strong>{{ $errors->first('status') }}</strong>
    </span>
    @endif
  </div><!-- form-group -->

<!-- </div> tab-pane -->

<hr>

<div class="row">
  <div class="col-md-6">
    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i>Salvar</button>
    <a href="{{ route('calendars.index') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i>Cancelar</a>
  </div><!-- col-md-6 -->
</div>