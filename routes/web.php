<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/webmail', function(){ return redirect('http://webmail.innsystem.com.br/'); });
Route::get('/admin', function(){ return redirect('/dashboard'); });

Route::get('/manutencao', 'Front\HomeController@maintence');
Route::get('/', 'Front\HomeController@suspenso');

Route::get('/sobre', 'Front\AboutController@index');

Route::get('categorias', 'Front\CategoriesController@index');
Route::get('categorias/{slugCategoria}', 'Front\CategoriesController@show');
Route::get('categorias/{slugCategoria}/{slugEstabelecimento}', 'Front\CategoriesController@showEstablishment');

Route::get('calendario/', 'Front\CalendarsController@index');
Route::get('calendario/{slugCalendario}', 'Front\CalendarsController@showCalendar');

Route::get('blogs', 'Front\BlogsController@index');
Route::get('blogs/{slugBlog}', 'Front\BlogsController@show');
Route::get('/depoimentos', 'Front\ReviewController@index');
Route::post('/depoimentos-create', 'Front\ReviewController@store');
Route::get('/contato', 'Front\ContactController@index');

Route::post('/estabelecimentos/busca', 'Front\HomeController@busca');


Route::prefix('services')->namespace('Services')->group(function() {
   Route::post('send-mail', 'MailController@sendContact');
});

Route::prefix('dashboard')->middleware(['auth'])->group(function(){
	Route::get('/', 'Admin\DashboardController@index');
	Route::resource('users', 'Admin\UsersController');
	Route::resource('pages', 'Admin\PagesController');
	Route::resource('configs', 'Admin\ConfigController');

    Route::resource('about', 'Admin\AboutController');
    Route::resource('blogs', 'Admin\BlogsController');
    Route::delete('blogs/{blog}/photos/{photo}', 'Admin\\BlogsController@photoRemove')->name('remove.photo');


    Route::resource('reviews', 'Admin\ReviewsController');
    Route::resource('sliders', 'Admin\SlidersController');
    Route::resource('categories', 'Admin\CategoriesController');
    Route::resource('establishments', 'Admin\EstablishmentsController');
    Route::delete('establishments/{establishment}/photos/{photo}', 'Admin\\EstablishmentsController@photoRemove')->name('remove.photo');

    Route::resource('calendars', 'Admin\CalendarsController');
    Route::delete('calendars/{calendar}/photos/{photo}', 'Admin\\CalendarsController@photoRemove')->name('remove.photo');

    Route::prefix('newsletter')->group(function() {
        Route::get('list', 'Admin\NewsletterController@index');
        Route::delete('/{newsletter}', 'Admin\NewsletterController@destroy');
        Route::get('generate-list', 'Admin\NewsletterController@generate');
        Route::view('/', 'admin.newsletter.index')->name('newsletter.index');
    });
    
});

Auth::routes();
